require('dotenv').config();
const app = require('./app');
const connectToDB = require('./src/db/connectToDB');
const scheduleDeleteFromPromotion = require('./src/scheduledTasks/scheduledDeleteFromPromotions');
const scheduledTasks = require('./src/scheduledTasks/scheduledTasks');
const sendBlueMail = require('./src/services/sendInBlue/index');
(async () => {
  try {
    app.listen(process.env.PORT, () =>
      console.log('listening on port ' + process.env.PORT || 8080)
    );
    const connection = await connectToDB(process.env.DB_URI);
    console.log(connection.message);
    scheduledTasks();
    //scheduleDeleteFromPromotion()
   
  } catch (err) {
    console.log('error index\n', err.message);
  }
})();
//===================
//test message
//===================
