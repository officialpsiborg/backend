const express = require('express');
const { updateProductGame } = require('../controllers/productControllers');
const webHookRoute = express.Router();

// userRoute.get('/getdetails', authenticateUser, getUserDetails);
webHookRoute.get('/gameavailability', updateProductGame)
module.exports = webHookRoute;
