const genAccessToken = require('./genAccessToken');
const isRefreshTokenIssued = require('./isRefreshTokenIssued');
const verifyRefreshToken = require('./verifyRefreshToken');

const newAccessToken = async (req, res, next) => {
  try {
    const refreshToken = req.params.refresh_token;
    // if (!refreshToken) {
    //   res.status(403).json({error: true, message: 'Refresh Token Missing'});
    // }
    const issued = await isRefreshTokenIssued(refreshToken);
    if (issued) {
      const user = verifyRefreshToken(refreshToken);
      const accessToken = genAccessToken(
        {
          _id: user._id,
          email: user.email,
          role: user.role,
        },
        '1h'
      );
      res.status(200).json({
        Error: false,
        Message: 'Access Token Generated.',
        AccessToken: accessToken,
        AccessTokenExpiry: '1h',
        User: {
          Email: user.email,
          Role: user.role,
        },
      });
    }
  } catch (err) {
    next(err);
  }
};

module.exports = newAccessToken;
