const ApiError = require("../../error-handler/ApiError")
const UserModel = require("../../models/UserModel")
const sendInBlue = require("../../services/sendInBlue")




const forgotPassword = async (req,res)=>{
    try{
    const {email} = req.body
    if(!email) return new ApiError("Invaid email")
        const user =  await UserModel.findOne({email:email})
    if(!user) return new ApiError("No User found for this particular email")
            // Reset Token Gen and add to database hashed (private) version of token
    const resetToken = user.getResetPasswordToken();
    await user.save();
        //create url for mail

   const resetURL = `${process.env.BASE_URL}resetPassword/${resetToken}`
    //==============send email ================//

    let emailConfig = {
        senderEmail: "no-reply@mckey.io",
        recieverEmail: email,
        subject: "Request for Password Reset",
        body: `
        we received a request to reset the password for your Mckey account.
        To reset your password, click on the following link:
        ${resetURL}
      `,
      };
    sendInBlue(emailConfig)
    .then(async () => {
      console.log("I am the user => ", user);
      return res
        .status(200)
        .json({ success: true, msg: "Email sent successfully" });
    })   .catch(async (error) => {
        console.log("I am the user from Error => ", user);
        console.log("Error from sendingmail in forgot password", error);
        user.resetPasswordToken = undefined;
        user.resetPasswordExpire = undefined;
        await user.save();
        return res.status(403).json({ msg: "Email not send" });
      });

    }catch(err){
        console.log("=============",err)
        return res.status(500).json({ status: false, error: err.message });
    }
}
module.exports = forgotPassword
