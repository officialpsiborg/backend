const registerUser = require('./registerUser');
const loginUser = require('./loginUser');
const logoutUser = require('./logoutUser');
const newAccessToken = require('./newAccessToken');
const forgotPassword =  require('./forgotPassword');
const resetPassword = require('./resetPassword');
const verifyEmail = require('./verifyEmail');

module.exports = {
  registerUser,
  loginUser,
  newAccessToken,
  logoutUser,
  forgotPassword,
  resetPassword,
  verifyEmail
};
