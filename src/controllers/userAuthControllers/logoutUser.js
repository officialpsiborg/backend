const RefreshToken = require('../../models/RefreshTokenModel');

const logoutUser = async (req, res, next) => {
  try {
    const refreshToken = req.params.refresh_token;
    const result = await RefreshToken.deleteOne({refreshToken});
    console.log('user logged out', result);
    res.status(204).send();
  } catch (error) {
    return next(error);
  }
};

module.exports = logoutUser;
