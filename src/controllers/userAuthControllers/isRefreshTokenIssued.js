const ApiError = require('../../error-handler/ApiError');
const RefreshToken = require('../../models/RefreshTokenModel');

const isRefreshTokenIssued = async (token) => {
  const refreshToken = await RefreshToken.findOne({refreshToken: token});
  if (!refreshToken) {
    throw new ApiError(
      `Token is invalid (not issued). Please login to continue.`,
      403
    );
  }
  return refreshToken;
};

module.exports = isRefreshTokenIssued;
