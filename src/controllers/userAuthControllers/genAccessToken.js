const jwt = require('jsonwebtoken');
require('dotenv').config();
const genAccessToken = (user, expiry) => {
  return jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, {expiresIn: expiry});
};

module.exports = genAccessToken;
