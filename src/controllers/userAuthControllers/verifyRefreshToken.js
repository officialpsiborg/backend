const jwt = require('jsonwebtoken');
const ApiError = require('../../error-handler/ApiError');
require('dotenv').config();

const verifyRefreshToken = (token) => {
  let decodedUser = null;
  let error = null;
  jwt.verify(token, process.env.REFRESH_TOKEN_SECRET, async (err, user) => {
    if (err) {
      error = new ApiError(
        `Refresh token is expired. Please login to continue.`,
        403
      );
    }
    decodedUser = user;
  });
  if (error) throw error;
  return decodedUser;
};
module.exports = verifyRefreshToken;
