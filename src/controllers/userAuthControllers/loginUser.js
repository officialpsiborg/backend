require('dotenv').config();
const User = require('../../models/UserModel');
const bcrypt = require('bcrypt');
const genAccessToken = require('./genAccessToken');
const genRefreshToken = require('./genRefreshToken');
const ApiError = require('../../error-handler/ApiError');

const loginUser = async (req, res, next) => {
  try {
    const {email, password} = req.body;
    const user = await User.findOne({email: email});
    if (!user) {
      throw new ApiError(`User with email ${email} does not exist`, 404);
    }
    //console.log("????",user)
    if(user.disabled===true ){
      throw new ApiError(`User with email ${email} has been disabled by admin,401`)
    }
    const verified = await bcrypt.compare(password, user.password);
    if (!verified) {
      throw new ApiError(`Invalid password`, 403);
    }
    if (verified) {
      console.log("process.env.JWT_EXPIRE ==>", typeof process.env.JWT_EXPIRE)
      const accessToken = genAccessToken(
        {
          _id: user._id,
          email: user.email,
          role: user.role,
        },
        process.env.JWT_EXPIRE
      );
      const refreshToken = await genRefreshToken(
        {
          _id: user._id,
          email: user.email,
          role: user.role,
        },
        process.env.JWT_REFRESH_EXPIRE
      );
      user.refreshToken = refreshToken; //for later use of http only cookie
      await user.save();
      res.status(200).json({
        Error: false,
        Message: 'Successfully logged in.',
        AccessToken: accessToken,
        AccessTokenExpiry: process.env.JWT_EXPIRE,
        RefreshToken: refreshToken,
        RefreshTokenExpiry: process.env.JWT_REFRESH_EXPIRE,
        User: {
          FirstName: user.firstName,
          LastName: user?.lastName,
          Email: user.email,
          Role: user.role,
          Phone: user?.phone,
          Verified: user.verified,
          CreatedAt: user.createdAt,
        },
      });
    }
  } catch (err) {
    return next(err);
  }
};

module.exports = loginUser;
