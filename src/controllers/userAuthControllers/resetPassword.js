const User = require("../../models/UserModel")
const resetPassword = async (req, res, next) => {

    // Compare token in URL params to hashed token
    const resetPasswordToken = crypto
      .createHash("sha256")
      .update(req.params.resetToken)
      .digest("hex");
    try {
      const user = await User.findOne({
        resetPasswordToken,
        resetPasswordExpire: { $gt: Date.now() },
      });
      if (!user) {
        return res.status(500).json({
          status: false,
          msg: "Invalid token",
        });
      }
      user.password = req.body.password;
      user.resetPasswordToken = undefined;
      user.resetPasswordExpire = undefined;
      await user.save();
      res.status(201).json({
        success: true,
        msg: "Password Updated Successfully",
      });
    } catch (err) {
      return res.status(500).json({ status: false, error: err.message });
    }
  };
  module.exports = resetPassword  