require('dotenv').config();
const jwt = require('jsonwebtoken');
const ApiError = require('../../error-handler/ApiError');

const verifyAccessToken = (token) => {
  if (!token) {
    throw new ApiError('Token Not Found', 401);
  }
  let decodedUser = null;
  let myErr = null;
  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
    if (err) {
      myErr = new ApiError('Access Token Invalid', 401);
      return;
    }
    decodedUser = user;
  });

  if (myErr) {
    throw myErr;
  }
  return decodedUser;
};

module.exports = verifyAccessToken;
