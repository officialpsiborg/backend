const jwt = require('jsonwebtoken');
const ApiError = require('../../error-handler/ApiError');
const RefreshToken = require('../../models/RefreshTokenModel');
require('dotenv').config();

const genRefreshToken = async (user, expiry) => {
  const token = jwt.sign(user, process.env.REFRESH_TOKEN_SECRET, {
    expiresIn: expiry,
  });
  const refreshToken = new RefreshToken({
    refreshToken: token,
  });
  try {
    await refreshToken.save();
    return token;
  } catch (err) {
    throw new ApiError(`Error while saving refresh token:${err.message}`, 400);
  }
};

module.exports = genRefreshToken;
