const User = require('../../models/UserModel');
const bcrypt = require('bcrypt');
const ApiError = require('../../error-handler/ApiError');
const sendBlueMail = require('../../services/sendInBlue/index')

const registerUser = async (req, res, next) => {
  try {
    const {firstName, lastName, email, password} = req.body;
    if (!firstName || !lastName || !email || !password) {
      return res.status(400).json({msg: "Provide all required data!"})
    }
    //validation on request
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(password, salt);
    const user = new User({
      firstName,
      lastName,
      email,
      password: hashedPassword,
    });

    const verifyUserEmailToken = user.getResetPasswordToken();


    const createdUser = await user.save();
    
    let emailConfig = {
      type : "Registration",
      senderEmail: "no-reply@mckey.io",
      recieverEmail: `${user.email}`,
      subject: "Welcome email for user",
      name: firstName,
      verificationLink: `${process.env.BASE_URL}verifyEmailToken/${verifyUserEmailToken}`
    };
    await sendBlueMail(emailConfig)
    res.status(201).json({
      Error: false,
      Message: 'User created successfully',
      User: {
        FirstName: createdUser.firstName,
        LastName: createdUser?.lastName,
        Email: createdUser.email,
      },
    });
  } catch (err) {
    if (err.code === 11000) {
      return next(
        new ApiError(
          `${req.body.email} is already registered! Please Login to continue`,
          409
        )
      );
    }
    return next(err);
  }
};

module.exports = registerUser;
