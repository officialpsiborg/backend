const User = require("../../models/UserModel");
const verifyEmail = async (req, res, next) => {
  console.log("verify email Called ==>");
  const { verifyEmailToken } = req.params;

  if (!verifyEmailToken) {
    return res
      .status(400)
      .json({ status: false, error: "Provide all required data" });
  }

  try {
    const user = await User.findOne({
      resetPasswordToken: verifyEmailToken,
      resetPasswordExpire: { $gt: Date.now() },
    });

    if (!user) {
      return res.status(500).json({
        status: false,
        msg: "Invalid token",
      });
    }

    if (user.verified) {
      return res
        .status(400)
        .json({ status: false, msg: "user is already Verified" });
    }

    user.resetPasswordToken = undefined;
    user.resetPasswordExpire = undefined;

    user.verified = true;
    user.save();
    // sendToken(user, 200, res);

    return res.status(200).json({ msg: "email verified successfully" });
  } catch (err) {
    return res.status(500).json({ status: false, error: err.message });
  }
};
module.exports = verifyEmail;
