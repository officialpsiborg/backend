const axios = require('axios');
const fetchProductFromGenba = require('../../genbaProductServices/ProductScheduler');

const {
    getAccessToken
  } = require('../../services/genbaServices');

const getGenbaGames = async (req,res, next)=>{
    // const result = await fetchProductFromGenba()

    // return res.status(200).json({msg: result.Games, length: result.totalGames})
    let token = await getAccessToken()

    const url =
    process.env.GENBA_BASE_URL +
    `/prices?sku=${req.query.Sku}`
    try{
        const axiores = await axios({
            method:"GET",
            url:url,
            headers:{
                Authorization: `Bearer ${token}`,
            }
        })

        // console.log("response from getGenbaGames() ==>",axiores.data)
        // return res.status(200).json({msg: res.data.products})
        res.status(200).json({
            message:"Bundle product fetch successfully",
            data:axiores.data
        })   
       
      } catch (err) {
        console.log("error from getGenbaGames  ===>", err)
        next(err)
      }
    
}


module.exports = getGenbaGames