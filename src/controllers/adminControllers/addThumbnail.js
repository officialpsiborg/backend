
const GenbaProdModel = require('../../models/GenbaProdModel');

const addThumbnail = async (req, res, next) => {
  try {
    const {gameId,banner} = req.body;
    
    const result = await GenbaProdModel.findById(gameId)
    result.banner = banner
    result.save()
    res.status(200).json({
      Error: false,
      Message: 'Banner updated successfully',
      // result: result,
    });
  } catch (err) {
    next(err);
  }
};



module.exports = addThumbnail;
