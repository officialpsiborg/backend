const GameCategory = require('../../models/GameCategoryModel');
const mongoose = require('mongoose');
const ApiError = require('../../error-handler/ApiError');

const addCategory = async (req, res, next) => {
  try {
    const {Name, NumberOfGames, Order, Games} = req.body;
    const {showgames, select} = req.query;
    if (!Name || !Order) {
      throw new ApiError('Category Name and Order is required', 400);
    }
    isInvalidObjectId(Games);
    const category = new GameCategory({
      Name,
      NumberOfGames,
      Order,
      Games,
    });

    const addedCategory = await category.save();
    if (showgames === 'true') {
      let selectedField = '';
      if (select) {
        selectedField = select.split(',').join(' ');
      }
      await addedCategory.populate('Games', selectedField);
    }
    res.status(201).json({
      Error: false,
      Message: 'Category added successfully',
      Category: addedCategory,
    });
  } catch (err) {
    if (err.code === 11000) {
      return next(
        new ApiError(`${req.body.Name} category is already exist.`, 409)
      );
    }
    next(err);
  }
};

function isInvalidObjectId(ids) {
  ids.forEach((id) => {
    if (!mongoose.Types.ObjectId.isValid(id)) {
      throw new ApiError('Invalid Object id: ' + id, 400);
    }
  });
}

module.exports = addCategory;
