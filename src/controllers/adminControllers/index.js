const getProducts = require("./getProducts");
const updateProduct = require("./updateProduct");
const addCategory = require("./addCategory");
const updateCategory = require("./updateCategory");
const requestApproval = require("./requestApproval");
const getTransactions = require("./getTransections");
const getUsers = require("./getUsers");
const addFeaturedGames = require("./addFeaturedGames");
const getSoldkeys = require("./getSoldKeys");
const getCategory = require("./getCategory");
const disableUser = require("./disableUser");
const deleteCategory = require("./deleteCategory");
const getFigures = require("./getFigures");
const updateAdmin = require("./updateAdmin");
const getNoGames = require("./getNoGames");
const getNoOrders = require("./getNoOrders");
const totalSale = require("./totalSale");
const recentOrders = require("./recentOrders");
const getNoUser = require("./getNoUser");
const deleteGameInCategory = require("./deleteGameInCategory");
const updateDisableProduct = require("./updateDisableProduct")
const passwordChange = require("./passwordChange")
const getfilterProduct = require("./getfilterProduct")
const getIdelProduct = require("./getIdelProduct")
const getLiveProduct = require("./getLiveProduct")
const graph =  require("./graph")
const gameTypeCount = require('./gameTypeCount');
const requestAllGames = require('./requestAllGame');
const addThumbnail = require('./addThumbnail');
const convertPriceInDb = require('./priceConvert')
const deleteUser = require('./deleteUser');

module.exports = {
  getProducts,
  updateProduct,
  addCategory,
  updateCategory,
  requestApproval,
  getTransactions,
  getUsers,
  addFeaturedGames,
  getSoldkeys,
  getCategory,
  disableUser,
  deleteCategory,
  getFigures,
  updateAdmin,  
  getNoGames,
  getNoOrders,
  totalSale,
  recentOrders,
  getNoUser,
  deleteGameInCategory,
  updateDisableProduct,
  passwordChange,
  getfilterProduct,
  getIdelProduct,
  getLiveProduct,
  graph,
  gameTypeCount,
  requestAllGames,
  addThumbnail,
  convertPriceInDb,
  deleteUser
};
