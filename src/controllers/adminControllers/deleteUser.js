const UserModel = require("../../models/UserModel")



const deleteUser = async (req,res, next)=>{

    const {userId} = req.params;
    try{
        const result =  await UserModel.findByIdAndDelete(userId)
        if(!result){
            throw new Error("No result found for user")
        }
        return res.status(200).json({
            msg:"User Deleted Successfully"
        })
    }catch(e){
        console.log("Error=====>",e)
        return res.status(500).json({msg: e.message})
    }
}

module.exports =  deleteUser