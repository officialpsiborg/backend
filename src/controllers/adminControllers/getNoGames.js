const GenbaProduct = require('../../models/GenbaProdModel');


const getNoGames =  async(req,res)=>{
    try{
        const counter =  await GenbaProduct.find({}).count()
        
        res.status(200).json({
            message: "Games found",
            games: counter
        })
    }catch(e){
        console.log(e)
    }
}
module.exports = getNoGames;