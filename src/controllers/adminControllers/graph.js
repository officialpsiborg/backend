const ApiError = require('../../error-handler/ApiError')
const Transaction = require('../../models/TransactionModel')
const graph =  async (req,res)=>{
    try {
        let start = req.query.start 
        let end =  req.query.end
        let KeyPrice = req.query.KeyPrice
        if(!start) start =""
        if(!end) end = ""
        // let findQuery = {}
       //
        // findQuery.createdAt = { $gte: new Date(new Date(start).setHours(00, 00, 00)),$lt: new Date(new Date(end).setHours(23, 59, 59))}
        // // $lt: new Date(new Date(endDate).setHours(23, 59, 59))
        // findQuery.txn_status = {$eq:"success"}
        // console.log("transaction",findQuery)
        // const result = await Transaction.find(findQuery)

        // testing 
        if (KeyPrice) {
            const result = await Transaction.aggregate([
                {
                    $match: {
                        createdAt : { 
                            $gte: new Date(new Date(start).setHours(00, 00, 00)),
                            $lt: new Date(new Date(end).setHours(23, 59, 59))
                        },
                        txn_status : {$eq:"success"}
                    }
                },
                {
                    $group: {
                        _id: { $dateToString: { format: "%Y-%m-%d", date: "$createdAt" } },
                        totalPrice: { $sum: "$txn_amt" }
                    }
                }
            ])
            if(!result){
                throw new ApiError("Couldn't find transaction in database")
            }
            res.status(200).json({
                message:"Transaction data",
                length:result.length,
                data:result
            })
        } else {
            const result = await Transaction.aggregate([
                {
                    $match: {
                        createdAt : { 
                            $gte: new Date(new Date(start).setHours(00, 00, 00)),
                            $lt: new Date(new Date(end).setHours(23, 59, 59))
                        },
                        txn_status : {$eq:"success"}
                    }
                },
                {
                    $group: {
                        _id: { $dateToString: { format: "%Y-%m-%d", date: "$createdAt" } },
                        // count: { $count: { } }
                        count: {"$sum": 1}
                    }
                }
            ])
            if(!result){
                throw new ApiError("Couldn't find transaction in database")
            }
            res.status(200).json({
                message:"Transaction data",
                length:result.length,
                data:result
            })
        }
    
    }catch(error){
        console.log("Error ================>",error)
    }
}
module.exports  =  graph