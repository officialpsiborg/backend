const ApiError = require("../../error-handler/ApiError");
const User = require('../../models/UserModel');

const updateAdmin = async (req,res,next)=>{
    try{
        
            let {firstName, lastName, phone,email} = req.body;
            const reqUser = req.user;
            const updateData = {};
            if (firstName) updateData.firstName = firstName
            if (lastName) updateData.lastName = lastName
            if (phone) updateData.phone = phone
            if(email) updateData.email = email
            console.log("update1",updateData)
            const updatedUser = await User.findOneAndUpdate(
              {
                email:email
              },
              updateData,
              {new: true}
            );
         //   updateData.save()
            if(!updatedUser){
                // throw new ApiError('Invalid Email', 400);
                return res.status(404).json({error: true, message: e.message})
            }
            console.log("updated2",updatedUser)
            res.status(201).json({
              Error: false,
              Message: 'User updated successfully',
              User: {
                     FirstName: updatedUser.firstName,
                     LastName: updatedUser.lastName,
                      Email: updatedUser.email,
                     Phone: updatedUser.phone,
                     Role: updatedUser.role,
                     Verified: updatedUser.verified,
              },
            });
            console.log("in admin update",updatedUser);
    }catch(e){
        console.log(e)
        return res.status(500).json({error: true, message: e.message})
    }
}
module.exports = updateAdmin;