const TransactionModel = require("../../models/TransactionModel")

const totalSale =   async(req,res)=>{
    try{
   
        // const sale = await TransactionModel.find({txn_status: 'success'}).select('txn_amt')
        // console.log("sale",sale)
       // const agg = sale.aggregate([{$group:{_id:null,txn_amount:{$sum:"$txn_amount"}}}])
       const sales = await TransactionModel.aggregate([{$match:{txn_status:"success"}},{ $group: { _id: null, txn_amt: { $sum: "$txn_amt" } } }])
       // const totalSale  =  sale.filter(x=>x.txn_amt)
        // console.log('-------->',sales)
        res.status(200).json({
            message:"TOTAL SALES",
            order:sales
        })
        
    }catch(e){
        console.log("ERROR +++",e)
    }


}
module.exports = totalSale;