const ApiError = require('../../error-handler/ApiError');

const User = require('../../models/UserModel');

const disableUser = async (req, res, next) => {
  try {
    const email = req.query.email;
    const {Disabled} = req.body;
    const updateData = {};
    if (Disabled != undefined) updateData.disabled = Disabled;
    if (!email) throw new ApiError('Bad Email Request', 400);

    const updatedUser = await User.findOneAndUpdate({email}, updateData, {
      new: true,
    });
    console.log("--------->>>>>",updatedUser)
    if (!updatedUser) throw new ApiError('User not found', 404);
    return res.status(201).json({
      Error: false,
      Message: 'User Details updated successfully',
    });
  } catch (error) {
    next(error);
  }
};

module.exports = disableUser;
