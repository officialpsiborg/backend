const GenbaProdModel = require("../../models/GenbaProdModel")

const addBundle = async (req,res)=>{
    try{
        const res = await GenbaProdModel.findOne({_id:req.params.id})
        res.status(200).json({
            message:"found the bundle",
            data:res.data
        })
    }catch(error){
        console.log("Error: " + error)
    }
}