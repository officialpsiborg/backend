const mongoose = require('mongoose');
const GenbaProduct = require('../../models/GenbaProdModel');
//admin
const getProducts = async (req, res, next) => {
  try {
    console.log("queary in getproducts",req.query)
    const {sku, query, platform, publisher, genres, select, live} = req.query;
    let {id, _id, pricefrom, priceto} = req.query;
    const findQuery = {};
    let genresArray = [];
    let selectedField = [];
    if (id) _id = id;
    if (_id) findQuery._id = _id;
    if (sku) findQuery.Sku = sku;
    findQuery.Srp =  {$exists:true}
    if (live != undefined) {
      findQuery.PreLiveState = live === 'false' ? {$in: [0, 2]} : 1;
    }
    if (platform) findQuery.Platform = platform.toUpperCase();
    if (publisher) findQuery.Publisher = publisher;
    // if (query) findQuery.Name = {$regex: `${query}`, $options: 'i'};
    if (query) {
      findQuery.$or = [
        {Name: {$regex: `${query}`, $options: 'i'}},
        {Publisher: {$regex: `${query}`, $options: 'i'}},
        {Developer: {$regex: `${query}`, $options: 'i'}},
      ];
    }
    

    if (pricefrom && priceto) {
      findQuery.Price = {
        $gte: parseFloat(pricefrom),
        $lte: parseFloat(priceto),
      };
    }
    if (pricefrom && !priceto) findQuery.Price = {$gte: parseFloat(pricefrom)};
    if (!pricefrom && priceto) findQuery.Price = {$lte: parseFloat(priceto)};

    if (genres) {
      genresArray = genres.split(',');
      findQuery.Genres = {$in: genresArray};
    }
   
  
    if (select) {
      selectedField = select.split(',').join(' ');
    }
    
   // findQuery.Srp = {Srp :{$exists:true}}
   
    const page = req.query.page || 1;
    // const limit = req.query.limit || 10;
    const limit = 9999999999;
    if (_id && !mongoose.Types.ObjectId.isValid(_id)) {
      return res.status(200).json({Error: false, Length: 0, Products: []});
    }
    selectedField + " Srp"
   
    const result = await GenbaProduct.find(findQuery)
      .skip((page - 1) * limit)
      .limit(limit)
      .select(selectedField);
   

    return res
      .status(200)
      .json({Error: false, Length: result.length, Products: result});
  } catch (error) {
    next(error);
  }
};

module.exports = getProducts;
