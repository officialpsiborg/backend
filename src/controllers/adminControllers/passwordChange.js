const ApiError = require("../../error-handler/ApiError");
const UserModel = require("../../models/UserModel");
const bcrypt = require('bcrypt');

const passwordChange =  async (req,res)=>{
    try{
        const email = req.body.email;
        const currentPassword  = req.body.currentPassword;
        const newPassword = req.body.newPassword;
        const user =  await UserModel.find({email:email})
        console.log("user in password",user[0].password)
        if(!user){
            // throw new ApiError(`Invalid email`,403) 
            return res.status(403).json({
                error: true,
                message:"Invalid email",    
               }) 
        }
        const verified = await bcrypt.compare(currentPassword, user[0].password);
        if (!verified) {
        //   throw new ApiError(`Invalid current password`, 403);
          return res.status(403).json({
            error: true,
            message:"Invalid current password",    
           }) 
        }
        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(newPassword, salt);
        const updateUser =  await UserModel.findOneAndUpdate({email:email},{password:hashedPassword})
        
        if(updateUser){
           res.status(200).json({
            message:"Admin password updated successfully",
            user: updateUser

           }) 
        }
 
    }catch(e){
        console.log("error=========>",e);
        return res.status(500).json({
            error: true,
            message:e.message,    
           }) 
    }
 


}
module.exports = passwordChange;
