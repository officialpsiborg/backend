//
const {default: mongoose} = require('mongoose');
const User = require('../../models/UserModel');

const getAdminProfile = async (req, res, next) => {
  try {
    console.log('get admin profile', req.user)
    const {_id, role} = req.user;

    if (role !== 'admin') {
        return res.status(200).json({Error: false, User: {}});
    }

    if (_id && !mongoose.Types.ObjectId.isValid(_id)) {
      return res.status(200).json({Error: false, User: {}});
    }

    // if (show_transactions && show_transactions === 'true') {
    //   let txnpage = req.query.txnpage || 1;
    //   let txnlimit = req.query.txnlimit || 5;
    //   populateTxn = {
    //     path: 'transactions',
    //     model: 'Transaction',
    //     select: '',
    //     options: {
    //       limit: txnlimit,
    //       skip: (txnpage - 1) * txnlimit,
    //       sort: {createdAt: -1},
    //     },
    //   };
    // }

    const user = await User.findById(_id)
      .select('-password')
    //   .populate(populateTxn ? populateTxn : '');
    res.status(200).json({Error: false, User: user});
  } catch (error) {
    next(error);
  }
};
module.exports = getAdminProfile;
