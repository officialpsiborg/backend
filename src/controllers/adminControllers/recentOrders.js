const TransactionModel = require("../../models/TransactionModel")


const recentOrders = async (req,res)=>{
    try{
   
    
        const result =  await TransactionModel.find({}).sort({createdAt: -1 }).limit(5).select('user_name txn_status prod createdAt txn_amt')
        if(!result){
            throw new Error("RESPONSE NOT FOUND")
        }
        res.status(200).json({
            message:"RECENT ORDER",
            orders:result
        })
    }catch(e){
        console.log(e)
    }
}
module.exports =  recentOrders