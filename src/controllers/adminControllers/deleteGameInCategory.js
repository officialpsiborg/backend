const GameCategoryModel = require("../../models/GameCategoryModel");



const deleteGameInCategory = async (req,res,next)=>{

try{
    const id = req.params.id;
    const gameId = req.body.gameId
    console.log("id of category",id)
    console.log("id of the game",gameId)
    const category = await GameCategoryModel.findOne({_id:id});
    const i = category.Games.findIndex((wish) => wish._id == gameId);
    i != -1 && category.Games.splice(i, 1);
    category.populate('Games');
    await category.save();
    res
      .status(200)
      .json({
        Error: false,
        Message: 'Product is removed from wishlist.',
        Games:category.Games,
      });
  } catch (error) {
    next(error);
  }
};

module.exports = deleteGameInCategory
