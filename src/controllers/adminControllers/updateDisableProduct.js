const GenbaProdModel = require("../../models/GenbaProdModel");



const updateDisableProduct = async (req,res)=>{
    try{
        const id = req.params.id;
        const data = req.body;
        console.log("data data data",data)
        console.log("id",id)
        const result =  await GenbaProdModel.findByIdAndUpdate(id,data)
        console.log("result of the update",result)
        res.status(200).json({
            message:"Updated GenbaProdModel successfully",
            data:result
        })
        
    }catch(e){
        console.log("Error updating product",e)
    }
}

module.exports =  updateDisableProduct