const mongoose = require('mongoose');
const Transaction = require('../../models/TransactionModel');
const getTransactions = async (req, res, next) => {
  try {
    //by txn details {_id, txn_oid, txn status,}
    //by user details {_id,email,} store email also
    //by prod details {_id, sku, keystatus, isDelivered, prodkey}
    //txn Ammount range date
    const {
      txn_id, //done---
      txn_oid, // done ----
      txn_status, //done
      user_id, // done
      // user_email, //
      prod_id, // done
      prod_sku, // done
      prod_key_status, // done
      key_delivered, //done
      prod_key, // done
      include_user,
      prod,
      query,
    } = req.query;

    const findQuery = {};
    if (txn_id) findQuery.txn_id = txn_id;
    if (txn_oid) findQuery.txn_oid = txn_oid;
    if (txn_status) findQuery.txn_status = txn_status;
    if (user_id) findQuery.user = user_id;
    // if(user_email) findQuery.user_email = user_email
    if (prod_id) findQuery.product = prod_id;
    if (prod_sku) findQuery.prod_sku = prod_sku;
    if (prod_key_status) findQuery.prod_key_status = prod_key_status;
    if (key_delivered != undefined) findQuery.isDelivered = key_delivered;
    if (prod_key) findQuery.prod_key = prod_key;
    if(prod) findQuery.prod = prod 

    if (query) {
      findQuery.$or = [
        {txn_oid: {$regex: `${query}`, $options: 'i'}},
        {user_name: {$regex: `${query}`, $options: 'i'}}
      ];
    }

    let populateUser;
    if (include_user && include_user === 'true') {
      populateUser = {
        path: 'user',
        model: 'User',
        select: 'firstName lastName email phone createdAt',
      };
    }

    const mongoIds = [];
    if (txn_id) mongoIds.push(txn_id);
    if (user_id) mongoIds.push(user_id);
    if (prod_id) mongoIds.push(prod_id);

    if (!isValidObjectIds(mongoIds)) {
      return res.status(200).json({
        Error: false,
        Length: 0,
        Transactions: [],
      });
    }

    const page = req.query.page || 1;
    const limit = req.query.limit || 10;

    const txns = await Transaction.find(findQuery)
      .sort({createdAt: -1})
      .skip((page - 1) * limit)
      .limit(limit)
      .populate(populateUser ? populateUser : '');
   // console.log("????check",txns)

   const totalDoc = await Transaction.countDocuments(findQuery)
    return res
      .status(200)
      .json({Error: false, Length: totalDoc || 0, Transactions: txns});
  } catch (error) {
    next(error);
  }
};

function isValidObjectIds(ids) {
  let valid = true;
  ids.forEach((id) => {
    if (!mongoose.Types.ObjectId.isValid(id)) {
      valid = false;
    }
  });
  return valid;
}

module.exports = getTransactions;
