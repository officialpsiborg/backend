const mongoose = require("mongoose");
const ApiError = require("../../error-handler/ApiError");
const GameCategory = require("../../models/GameCategoryModel");

const addFeaturedGames = async (req, res, next) => {
  try {
    const { games } = req.body;
    const { select } = req.query;
    if (!games || !Array.isArray(games) || games.length <= 0) {
      throw new ApiError("Games must be an array of games id.", 400);
    }
    games.forEach((id) => {
      if (!mongoose.Types.ObjectId.isValid(id)) {
        throw new ApiError("Invalid Object id: " + id, 400);
      }
    });
    let selectedFields = "";
    if (select) {
      selectedFields = select.split(",").join(" ");
    }
    const featuredGamesCategory = await GameCategory.findOneAndUpdate(
      { Name: "FeaturedGames" },
      // { Order: 1 },
      { Games: games },
      { new: true, upsert: true }
    );
    await featuredGamesCategory.populate("Games", selectedFields);
    return res.status(201).json({
      Error: false,
      Message: "Featured Games Added Successfully",
      FeaturedGames: featuredGamesCategory.Games,
    });
  } catch (error) {
    next(error);
  }
};

module.exports = addFeaturedGames;

/*

 try {
    const {games} = req.body;
    let select = req.query.select;
    let selectedField = '';
    if (!games || !Array.isArray(games) || games.length <= 0) {
      throw new ApiError('Games must be an array of games id.', 400);
    }
    if (select) {
      selectedField = select.split(',').join(' ');
    }
    const featuredGames = new FeaturedGames({
      FeaturedGames: games,
    });
    const result = await featuredGames.save();
    await result.populate('FeaturedGames', selectedField);
    return res.status(200).json({
      Error: false,
      Message: 'Featured Games Added Successfully',
      FeaturedGames: result.FeaturedGames,
    });
  } catch (error) {
    next(error);
  }

*/
