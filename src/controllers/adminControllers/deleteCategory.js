const ApiError = require('../../error-handler/ApiError');
const GameCategory = require('../../models/GameCategoryModel');
const mongoose = require('mongoose');

const deleteCategory = async (req, res, next) => {
  try {
    const id = req.params.id;
    if (id && !mongoose.Types.ObjectId.isValid(id)) {
      throw new ApiError('Category not found', 404);
    }
    const category = await GameCategory.findOneAndDelete({_id: id});
    res.status(200).json({
      Error: false,
      Message: 'Category Deleted successfully',
    });
  } catch (err) {
    next(err);
  }
};

module.exports = deleteCategory;
