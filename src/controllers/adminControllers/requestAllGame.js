
const {requestAllApprovalForProduct} = require('../../services/genbaServices');

const requestAllGames = async (req, res, next) => {
  try {
    let resp = await requestAllApprovalForProduct()

    if (!resp) {
      res.status(400).json({
        Error: true,
        Message: "Something went wrong",
      });
    }

    return res.status(200).json({
        Error: false,
        Message: "Requested Submitted Successfully",
    })


  } catch (error) {
    next(error);
  }
};

module.exports = requestAllGames;
