const Transaction = require('../../models/TransactionModel');
const mongoose = require('mongoose');
const ApiError = require('../../error-handler/ApiError');
const getSoldkeys = async (req, res, next) => {
  try {
    const {id, sku, query} = req.query;
    if (!id && !sku) throw new ApiError('Id or Sku is Required', 400);
    if (id && !mongoose.Types.ObjectId.isValid(id)) {
      throw new ApiError('Product id is invalid', 400);
    }
    const findQuery = {
      isDelivered: true,
      prod_key_status: 'delivered',
      prod_key: {$exists: true},
    };
    if (id) findQuery.product = id;
    if (sku) findQuery.prod_sku = sku;

    if (query) {
      findQuery.$or = [
        {firstName: {$regex: `${query}`, $options: 'i'}},
        {lastName: {$regex: `${query}`, $options: 'i'}},
        {email: {$regex: `${query}`, $options: 'i'}},
      ];
    }

    const page = req.query.page || 1;
    const limit = req.query.limit || 10;

    const result = await Transaction.find(findQuery).populate({ path: "user", select: 'firstName lastName email' }).skip((page - 1) * limit)
    .limit(limit);

    const total = await Transaction.countDocuments(findQuery)

    return res
      .status(200)
      .json({Error: false, Length: total, Transactions: result});
  } catch (error) {
    next(error);
  }
};

module.exports = getSoldkeys;
