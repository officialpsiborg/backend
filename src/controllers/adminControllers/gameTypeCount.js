const mongoose = require('mongoose');
const GenbaProduct = require('../../models/GenbaProdModel');
//admin
const gameTypeCount = async (req, res, next) => {
  try {
    
    const result = await GenbaProduct.aggregate([
        {
            $group: 
        {
            _id: "$PreLiveState",
            count: { $sum: 1 }
        }
        }
    ])

    return res
      .status(200)
      .json({Error: false, Length: 1 || 23, Products:result});
  } catch (error) {
    next(error);
  }
};

module.exports = gameTypeCount;
