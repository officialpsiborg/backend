const ApiError = require('../../error-handler/ApiError');
const GenbaProduct = require('../../models/GenbaProdModel');
const mongoose = require('mongoose');
const { update } = require('../../models/GenbaProdModel');

const updateProduct = async (req, res, next) => {
  try {
    const {id, sku, select} = req.query;

    if (!id && !sku) {
      throw new ApiError('Product id or Product sku is required', 400);
    }

    const findData = {};
    if (id) findData._id = id;
    if (sku) findData.Sku = sku;

    const {Price, Discount} = req.body;
    console.log("in updateProduc=============????",Discount, Price)
    const updateData = {};
    if (Price) updateData.Price = Price;
    if (Discount) {
      updateData.Discount = Discount
      updateData.finalDiscount = Discount
    }
    //if (Disabled != undefined) updateData.Disabled = Disabled;


   // console.log("finddata",findData)
   // console.log("Update data",updateData)
    const data =  await GenbaProduct.findOne({_id:id})
    // console.log("-----",data)
    let srp = data.Srp * Discount/100
    let pri =  data.Srp - srp
    // console.log("SRP type of", srp)
  //  console.log("Discount type of", Discount)
  //  console.log("TOTAL DISCOUNT",pri)
   let finalPrice =   Math.round(pri)
   let finalDiscount = Discount
   
   //  console.log("data finalPrice ==>", data.finalPrice)
   //  console.log("data finalDiscount ===>", data.finalDiscount)
   if (data.onPromotion) {

      let srpPromo = data.onPromotionPrice * Discount/100
      let priPromo =  data.onPromotionPrice - srpPromo
      let finalPricePromo =   Math.round(priPromo)
      finalPrice = finalPricePromo
    //  finalPrice = (data.finalPrice - (data.finalPrice* (data.finalDiscount/100)))
     console.log("admin Discount Price  ==>", finalPricePromo, data.Srp)
     finalDiscount = Math.round(((data.Srp - finalPricePromo)/data.Srp) * 100)
     console.log("final Discount ===>", finalDiscount)
    }
    // console.log("pricesssssssssssssss",finalPrice, pri)
    // console.log("finalDiscounttttttttt", Math.round(finalDiscount))

    const updatePrice =  await GenbaProduct.findOneAndUpdate(findData,{Discount:Discount,Price:finalPrice, isAdminPromotion: true, finalDiscount, finalPrice}).select("Name Price Discount")

    // console.log("updatea data ",updatePrice)

    return res.status(201).json({
      Error: false,
      Message: 'Product updated successfully',
      Product: updatePrice,
    });
   }catch (error) {
    next(error);
  }
};
module.exports = updateProduct;
