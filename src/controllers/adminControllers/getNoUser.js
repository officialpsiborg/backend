const UserModel = require("../../models/UserModel")



const getNoUser = async (req,res)=>{
    try{
        const result =  await UserModel.find({}).count()
        if(!result){
            throw new Error("No result found for user")
        }
        res.status(200).json({
            message:"USER FOUND",
            User:result
        })
    }catch(e){
        console.log("Error=====>",e)
    }
}

module.exports =  getNoUser