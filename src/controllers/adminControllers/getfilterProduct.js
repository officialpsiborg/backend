const GenbaProduct = require('../../models/GenbaProdModel');

const getfilterProduct =  async (req,res)=>{
    try {

    
        let {id, _id, pricefrom, priceto,genres} = req.query;
        const findQuery = {};
        let genresArray = [];
        let selectedField = [];
   
        // if (query) findQuery.Name = {$regex: `${query}`, $options: 'i'};


    
        // if (priceto) {
        //   priceto = parseFloat(priceto);
        //   pricefrom = pricefrom ? parseFloat(priceto) : 0;
        //   findQuery.Price = {$gt: pricefrom, $lte: priceto};
        // }
    
        if (pricefrom && priceto) {
          findQuery.Price = {
            $gte: parseFloat(pricefrom),
            $lte: parseFloat(priceto),
          };
        }
        if (pricefrom && !priceto) findQuery.Price = {$gte: parseFloat(pricefrom)};
        if (!pricefrom && priceto) findQuery.Price = {$lte: parseFloat(priceto)};
    
        if (genres) {
          genresArray = genres.split(',');
          findQuery.Genres = {$in: genresArray};
        }
        findQuery.Srp = {$exists : true}
    
       
        const page = req.query.pageno || 1;
        const limit = req.query.limit || 10;
        if (_id && !mongoose.Types.ObjectId.isValid(_id)) {
          return res.status(200).json({Error: false, Length: 0, Products: []});
        }
        const result = await GenbaProduct.find(findQuery).skip((page - 1) * limit).limit(limit).select(selectedField);
         // console.log(result)
         // const temp = result.filter(x=>x.Srp )
        res.status(200).json({Error: false, Length: result.length, Products: result});
      } catch (err) {
        console.log("????",err)
      }
    

}
module.exports =  getfilterProduct