const GenbaProduct = require('../../models/GenbaProdModel');
const User = require('../../models/UserModel');
const Transaction = require('../../models/TransactionModel');

const getFigures = async (req, res, next) => {
  try {
    const totalPro = await GenbaProduct.countDocuments({});
    const totalUser = await User.countDocuments({});
    const totalOrders = await Transaction.countDocuments({isDelivered: true});
    res.status(200).json({
      Error: false,
      TotalGames: totalPro,
      TotalKeySold: totalOrders,
      TotalUsers: totalUser,
    });
  } catch (error) {
    next(error);
  }
};

module.exports = getFigures;
