const GenbaProdModel = require('../../models/GenbaProdModel');
const {requestApprovalForProduct} = require('../../services/genbaServices/requestApprovalForProduct');

const requestApproval = async (req, res, next) => {
  try {
    let sku =  req.query.sku;
    let product =  await GenbaProdModel.findOne({Sku:sku})
    console.log("check request product",product._id)
    const result = await requestApprovalForProduct(sku,product._id);
    console.log('result ', result);
    if (!result.error) {
      let message = 'Unable to request for approval for Product';
      if (result.liveState == 0)
        message = 'Product Approval Requested successfully, Approval Pending';
      if (result.liveState == 1) message = 'Product is Approved';
      if (result.liveState == 2) message = 'Product Approval is Denied';
      res.status(200).json({
        Error: false,
        Message: message,
      });
    }
    if (result.error) {
      res.status(400).json({
        Error: true,
        Message: result.errorMessage,
      });
    }
  } catch (error) {

    next(error);
  }
};

module.exports = requestApproval;
