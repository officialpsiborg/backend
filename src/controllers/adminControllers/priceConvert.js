
const UserModel = require('../../models/UserModel');

const convertPriceInDb = async (req, res, next) => {
  try {
    const {amount} = req.body;
    
    const result = await UserModel.findByIdAndUpdate(req.user._id, {convertPrice: amount})

    res.status(200).json({
      Error: false,
      Message: 'Price updated successfully',
      result: result,
    });
  } catch (err) {
    next(err);
  }
};



module.exports = convertPriceInDb;
