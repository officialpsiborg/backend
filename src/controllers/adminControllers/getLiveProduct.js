
const ApiError = require('../../error-handler/ApiError');
const GenbaProdModel = require('../../models/GenbaProdModel');

const getLiveProduct = async (req,res)=>{
    try {

    
        let {id, _id, pricefrom, priceto,genres, sortPrice, 
          sortName, sortDiscount, query, sortPricePattern, sortDiscountPattern, 
          sortNamePattern
        } = req.query;
        const findQuery = {};
        let genresArray = [];
        let selectedField = [];
        let sortQuery = {};
        // sortQuery.ReleaseDate = -1
   
        if (query) findQuery.Name = {$regex: `${query}`, $options: 'i'};


    
        // if (priceto) {
        //   priceto = parseFloat(priceto);
        //   pricefrom = pricefrom ? parseFloat(priceto) : 0;
        //   findQuery.Price = {$gt: pricefrom, $lte: priceto};
        // }
    
        if (pricefrom && priceto) {
          findQuery.Price = {
            $gte: parseFloat(pricefrom),
            $lte: parseFloat(priceto),
          };
        }
        if (pricefrom && !priceto) findQuery.Price = {$gte: parseFloat(pricefrom)};
        if (!pricefrom && priceto) findQuery.Price = {$lte: parseFloat(priceto)};
    
        if (sortPrice)  sortQuery.finalPrice = Number(sortPrice)
        if (sortName)  sortQuery.Name = Number(sortName)
        if (sortDiscount)  sortQuery.Name = Number(sortDiscount)
        
        if (genres) {
          genresArray = genres.split(',');
          findQuery.Genres = {$in: genresArray};
        }
        // findQuery.Srp = {$exists : true}
        findQuery.PreLiveState= {$eq:1}

        if (sortPricePattern && sortPricePattern !== "undefined") sortQuery.finalPrice = Number(sortPricePattern) ? 1 : -1
        if (sortDiscountPattern && sortDiscountPattern !== "undefined") sortQuery.finalDiscount = Number(sortDiscountPattern) ? 1 : -1
        if (sortNamePattern && sortNamePattern !== "undefined") sortQuery.Name = Number(sortNamePattern) ? 1 : -1
       
        console.log("sortQuery Live ==>", sortQuery)

       
        const page = req.query.pageno || 1;
        const limit = req.query.limit || 10;
        if (_id && !mongoose.Types.ObjectId.isValid(_id)) {
          return res.status(200).json({Error: false, Length: 0, Products: []});
        }
        let result = GenbaProdModel.find(findQuery).sort(sortQuery).skip((page - 1) * limit).limit(limit).select(selectedField);
        result  = await result.sort({ReleaseDate: -1})
        const totalDoc = await GenbaProdModel.countDocuments(findQuery)
        // console.log(result)
         // const temp = result.filter(x=>x.Srp )
        res.status(200).json({Error: false, Length: totalDoc || 0, Products: result});
      } catch (err) {
        console.log("????",err)
        // next(err)
        return res.status(500).json({msg: err.message })
      }
    
}
module.exports = getLiveProduct