//
const {default: mongoose} = require('mongoose');
const User = require('../../models/UserModel');

const getUsers = async (req, res, next) => {
  try {
    const {user_id, user_email, disabled, show_transactions, query, sortUserName} = req.query;

    const findQuery = {role: 'user' };
    let sortQuery = {createdAt: -1};
    let populateTxn;
    if (user_id) findQuery._id = user_id;
    if (user_email) findQuery.email = user_email;
    if (disabled != undefined) findQuery.disabled = disabled;

    if (query) {
      findQuery.$or = [
        {firstName: {$regex: `${query}`, $options: 'i'}},
        {lastName: {$regex: `${query}`, $options: 'i'}}
      ];
    }

    if (sortUserName) sortQuery.firstName = sortUserName

    if (user_id && !mongoose.Types.ObjectId.isValid(user_id)) {
      return res.status(200).json({Error: false, Length: 0, Users: []});
    }
    if (show_transactions && show_transactions === 'true') {
      let txnpage = req.query.txnpage || 1;
      let txnlimit = req.query.txnlimit || 5;
      populateTxn = {
        path: 'transactions',
        model: 'Transaction',
        select: '',
        options: {
          limit: txnlimit,
          skip: (txnpage - 1) * txnlimit,
          sort: {createdAt: -1},
        },
      };
    }
    const page = req.query.page || 1;
    const limit = req.query.limit || 10;

    const users = await User.find(findQuery).sort(sortQuery)
      .skip((page - 1) * limit)
      .limit(limit)
      .select('-password')
      .populate(populateTxn ? populateTxn : '');

      const totalDoc = await User.countDocuments(findQuery)
    res.status(200).json({Error: false, Length: totalDoc || 0, Users: users});
  } catch (error) {
    next(error);
  }
};
module.exports = getUsers;
