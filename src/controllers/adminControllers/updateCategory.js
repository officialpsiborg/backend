const ApiError = require("../../error-handler/ApiError");
const GameCategory = require("../../models/GameCategoryModel");
const mongoose = require("mongoose");
const updateCategory = async (req, res, next) => {
  try {
    const _id = req.params._id;
    if (_id && !mongoose.Types.ObjectId.isValid(_id)) {
      throw new ApiError("Category not found", 404);
    }
    // const {Name, NumberOfGames, Order, AddedGames, DeletedGames} = req.body;
    const { Name, NumberOfGames, Order, Games } = req.body;
    console.table(req.body);
    const { select } = req.query;
    // if (DeletedGames && DeletedGames.length > 0) {
    //   isInvalidObjectId(DeletedGames);
    // }
    if (Games && Games.length > 0) {
      isInvalidObjectId(Games);
    }
    // if (AddedGames && AddedGames.length > 0) {
    //   isInvalidObjectId(AddedGames);
    // }

    const category = await GameCategory.findById(_id);
    if (!category) throw new ApiError("Category not found", 404);
    if (Name) category.Name = Name;
    if (NumberOfGames) category.NumberOfGames = NumberOfGames;
    if (Order) category.Order = Order;
    if (Games) category.Games = Games;

    // if (AddedGames && AddedGames.length > 0) {
    //   AddedGames.forEach((id) => {
    //     if (!category.Games.includes(id)) category.Games.unshift(id);
    //   });
    // }
    // if (DeletedGames && DeletedGames.length > 0) {
    //   DeletedGames.forEach((id) => {
    //     const i = category.Games.indexOf(id);
    //     i > -1 && category.Games.splice(i, 1);
    //   });
    // }
    await category.save();
    let selectedFields = "";
    if (select) {
      selectedFields = select.split(",").join(" ");
    }
    await category.populate("Games", selectedFields);
    res.status(201).json({
      Error: false,
      Message: "Category updated successfully",
      Category: category,
    });
  } catch (err) {
    if (err.code === 11000) {
      return next(
        new ApiError(`${req.body.Name} category is already exist.`, 409)
      );
    }
    next(err);
  }
};

function isInvalidObjectId(ids) {
  ids.forEach((id) => {
    if (!mongoose.Types.ObjectId.isValid(id)) {
      throw new ApiError("invalid id: " + id);
    }
  });
}

module.exports = updateCategory;

// if (DeletedGames && DeletedGames.length > 0) {
//   isInvalidObjectId(DeletedGames);
//   updateData.$pull = {
//     Games: {$in: DeletedGames},
//   };
// }

// if (AddedGames && AddedGames.length > 0) {
//   isInvalidObjectId(AddedGames);
//   updateData.$push = {
//     Games: {$each: AddedGames},
//   };
// }
// if (
//   AddedGames &&
//   AddedGames.length > 0 &&
//   DeletedGames &&
//   DeletedGames.length > 0
// ) {
//   throw new ApiError(
//     'Only one operation can be performed either Add Games or Delete Games',
//     400
//   );
// }
