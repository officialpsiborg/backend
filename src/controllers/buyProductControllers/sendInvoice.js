const sendInBlue = require("../../services/sendInBlue");

const sendInVoice = async (email, name, games, orderId, time, totalAmount ) => {
    let emailConfig = {
        senderEmail: "no-reply@mckey.io",
        recieverEmail: `${email}`,
        subject: "Purchase Order for user",
        type: "OrderInvoice",
        name: name,
        games: games,
        orderId: orderId,
        time: time,
        totalAmount: totalAmount
      };
      await sendInBlue(emailConfig).then(
        (data) => console.log(`Order Invoice send to ${email} <==> ${data}`)
        ).catch((err) => console.log("Error from Order Invoice send Email =>", err))
};

module.exports = sendInVoice;
