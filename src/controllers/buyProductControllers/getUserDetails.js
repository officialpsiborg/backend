const User = require('../../models/UserModel');

const getUserDetails = async (id, email, selectedString) => {
  try {
    const findQuery = {};
    if (id) findQuery._id = id;
    if (email) findQuery.email = email;
    return await User.findOne(findQuery).select(selectedString);
  } catch (error) {
    console.log(error.message);
    return;
  }
};
module.exports = getUserDetails;
