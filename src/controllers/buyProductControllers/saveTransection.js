const Transection = require('../../models/TransactionModel');

const saveTrasection = async (
  txn_oid,
  txn_amt,
  user_name,
  user,
  //prod_name,
  //prod_sku,
  product,
  prod
) => {
  const txn = new Transection({
    txn_oid,
    txn_amt,
    user_name,
    user,
    //prod_name,
    //prod_sku,
    product,
    prod
  });
  const createdTxn = await txn.save();
  
  return createdTxn;
  // return {
  //   _id: createdTxn._id,
  //   txn_oid: createdTxn.txn_oid,
  //   txn_amt: createdTxn.txn_amt,
  //   prod_name: createdTxn.prod_name,
  //   prod_id: createdTxn.prod_id,
  //   txn_status: createdTxn.txn_status,
  //   prod_key_status: createdTxn.prod_key_status,
  //   isDelivered: createdTxn.isDelivered,
  // };
};

module.exports = saveTrasection;
