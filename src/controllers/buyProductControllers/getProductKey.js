const axios = require('axios');
const ApiError = require('../../error-handler/ApiError');
const GenbaProduct = require('../../models/GenbaProdModel');
// const Order = require('../../models/OrderModel');
const Transaction = require('../../models/TransactionModel');
const User = require('../../models/UserModel');
const orderProductKey = require('../userControllers/orderProductKey');
const sendInVoice = require('./sendInvoice');
const getUserDetail = require("./getUserDetails");
const GenbaProdModel = require('../../models/GenbaProdModel');


const getProductKey = async (req, res, next) => {
  try {
    const txnOid = req.params.txnOid;
    const reqUser = req.user;
    const user = await getUserDetail(
      '',
      reqUser.email,
      '_id firstName lastName email phone transactions cart'
    );
    const txn = await Transaction.findOne({txn_oid: txnOid}).populate('prod.productId user');
    // console.log("txn From getProduct Key ==>", txn)
    // orderProductKey(txn);
    if (!txn) {
      throw new ApiError(
        `No any transaction found for transaction id "${txnOid}"`,
        400
      );
    }
    if (txn.txn_status === 'success' && txn.isDelivered) {
      return res.status(200).json({
        Error: false,
        Message:
          'Transaction is already successful And Key is already delivered',
        Transaction: txn,
      });
    }

    // txn.txn_status == 'initial' ||
    // txn.txn_status == 'pending' ||
    // txn.txn_status == 'challenge' ||
    // txn.txn_status == 'failure'

    //checking status from midtrans api
    // for sandbox
    // `https://api.sandbox.midtrans.com/v2/${txn.txn_oid}/status`,
    try {
      const response = await axios({
        method: 'GET',
        url: `https://api.midtrans.com/v2/${txn.txn_oid}/status`,
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Basic ${authString()}`,
        },
      });
    //  console.log('txn status res', response.data);

      const {status_code, transaction_status, fraud_status, transaction_id} =
        response.data;
        
      if (status_code == 200 || status_code == 201 || status_code == 202) {
        txn.txn_id = transaction_id;
        if (transaction_status == 'capture') {
          if (fraud_status == 'challenge') txn.txn_status = 'challenge';
          if (fraud_status == 'deny') txn.txn_status = 'failure';
          if (fraud_status == 'accept') {
            txn.txn_status = 'success'
            console.log("sending MAil txn was success")
            await sendInVoice(txn.user.email, txn.user.name, txn.prod, txn.txn_oid, new Date(), txn.txn_amt)
            user.transactions.unshift(txnOid);
            user.cart = {
              subtotal: 0,
              totalItem: 0,
              items: []
            }

            await user.save()
          };
        }
        if (transaction_status == 'settlement') txn.txn_status = 'success';
        if (
          transaction_status == 'cancel' ||
          transaction_status == 'deny' ||
          transaction_status == 'expire'
        )
          txn.txn_status = 'failure';
        if (transaction_status == 'pending') txn.txn_status = 'pending';

        await txn.save();
        if (
          txn.txn_status == 'success' &&
          !txn.isDelivered &&
          !txn.prod_key.length &&
          txn.prod_key_status != 'delivered'
        ) {
          const valueKey = await orderProductKey(txn);
         console.log("KEY IS HERE " , valueKey) // [{Code: "", KeyType: "", Sku: "", ProductName: ""}]
          let key = []
         for (let item of valueKey) {
            key.push(item.Code)
         }
         let prodKey = await mapByid(valueKey, txn.prod)
         
          if (valueKey) {
            txn.prod_key = [...key];
            txn.prod_key_status = 'delivered';
            txn.prod = prodKey;
            txn.isDelivered = true;
            await txn.save();
            await User.findOneAndUpdate(
              {email: reqUser.email},
              {$inc: {gamesOwned: 1}} //increase games owned by one to the current user
            );
            await GenbaProduct.findOneAndUpdate(
              {Sku: txn.prod_sku},
              {$inc: {KeySold: 1}} //increase key sold by one
            );
            return res.status(200).json({
              Error: false,
              Transaction: txn,
              Message: 'New Product Key Delivered',
            });
          } else {
            txn.prod_key_status = 'pending';
            await txn.save();
            return res.status(200).json({
              Error: false,
              Transaction: txn,
              Message: 'Unable to get product key try after some time.',
            });
          }
        }
      }
      // status_code == 404(txn not exist) or any other status code
      return res.status(200).json({
        Error: false,
        Transaction: txn,
        Message: 'Transaction is in Idle State',
      });
    } catch (error) {
      return res.status(200).json({
        Error: false,
        Transaction: txn,
        Message:
          ' [Test msg] Transaction status fetch error on midtrans not success response' +
          'errMsg' +
          error.message +
          ' errRESdata' +
          error?.response?.data,
      });
    }
  } catch (error) {
    next(error);
  }
};


async function mapByid(value, compareTo) { 
  let arr = [];
  for (let val of compareTo) {
    let found = value.findIndex((item)=> item.Sku === val.Sku)
    if (found >= 0) {
      val.prod_key = value[found].Sku
      val.prod_key_status = "delivered"
      arr.push(val)
    }

  }

  return arr
}

function authString() {
  try {
    console.log('try auth string gen');
    return btoa(process.env.PAYMENT_SERVER_KEY + ':');
  } catch (e) {
    console.log('catch auth string gen');
    return Buffer.from(process.env.PAYMENT_SERVER_KEY + ':').toString('base64');
  }
}

module.exports = getProductKey;

//----------------------------------------------------------------

// const axios = require('axios');
// const ApiError = require('../../error-handler/ApiError');
// const Transaction = require('../../models/TransactionModel');

// const getProductKey = async (req, res, next) => {
//   try {
//     const txnOid = req.params.txnOid;
//     const txn = await Transaction.findOne({txn_oid: txnOid});
//     if (!txn)
//       throw new ApiError(
//         `No any transaction found for transaction id "${txnOid}"`
//       );
//     if (txn.txn_status == 'pending') {
//       //check status of transaction
//       const authString = () => {
//         try {
//           console.log('try auth');
//           return btoa(process.env.PAYMENT_SERVER_KEY + ':');
//         } catch (e) {
//           console.log('catch auth');
//           return Buffer.from(process.env.PAYMENT_SERVER_KEY + ':').toString(
//             'base64'
//           );
//         }
//       };
//       try {
//         const response = await axios({
//           method: 'GET',
//           url: `https://api.sandbox.midtrans.com/v2/${txnOid}/status`,
//           headers: {
//             Accept: 'application/json',
//             'Content-Type': 'application/json',
//             Authorization: `Basic ${authString()}`,
//           },
//         });
//         const {status_code, transaction_status} = response.data;
//         if (status_code == 200 && transaction_status == 'capture') {
//           const updatedTxn = await Transaction.findOneAndUpdate(
//             {txn_oid: txnOid},
//             {txn_status: 'success'},
//             {new: true}
//           );
//           return res.status(200).json({
//             error: false,
//             transaction: updatedTxn,
//           });
//         }
//       } catch (error) {
//         console.log('Error in status fetch', error.message);
//         return res.status(200).json({
//           error: false,
//           transaction: txn,
//         });
//       }
//     }

//     return res.status(200).json({
//       error: false,
//       transaction: txn,
//     });
//   } catch (error) {
//     next(error);
//   }
// };

// module.exports = getProductKey;
