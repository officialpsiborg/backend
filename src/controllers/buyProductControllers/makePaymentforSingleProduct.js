require("dotenv").config();
// const {v4: uuid} = require('uuid');
const midtransClient = require("midtrans-client");
const getProductDetails = require("./getProductDetails");
const ApiError = require("../../error-handler/ApiError");
const { getStockOfProduct } = require("../../services/genbaServices");
const getUserDetails = require("./getUserDetails");
const saveTrasection = require("./saveTransection");
const User = require("../../models/UserModel");

const { v1: uuidv1, v4: uuidv4 } = require("uuid");
const sendInVoice = require("./sendInvoice");
const { default: mongoose } = require("mongoose");
const GenbaProduct = require("../../models/GenbaProdModel");
const snap = new midtransClient.Snap({
  isProduction: true,
  serverKey: process.env.PAYMENT_SERVER_KEY,
});

async function mapByid(item) {
  return item.map((item) => {
    return {
      unid: "uni-" + uuidv1(),
      productId: item._id,
      amount: item.finalPrice,
      Sku: item.Sku,
      Name: item.Name,
      prod_key: "",
      prod_key_status: "",
    };
  });
}

function itemsDetails(items) {
  return items.map((item) => {
    return {
      id: item._id,
      name: item.Name,
      sku: item.Sku,
      quantity: 1,
      price: item.finalPrice,
    };
  });
}

const makePaymentforSingleProduct = async (req, res, next) => {
  try {
    console.log("Make PAyment Function called ==>");
    // const sku = req.params.sku;
    const reqUser = req.user;
    const { productId } = req.params;
    if (!mongoose.Types.ObjectId.isValid(productId)) {
      return res.status(400).json({
        Error: false,
        Message: "product Id is not in correct format",
      });
    }

    const getProductDetail = await GenbaProduct.findById(productId);

    // get user fname lname email phone
    const user = await getUserDetails(
      "",
      reqUser.email,
      "_id firstName lastName email phone transactions cart"
    );

    // const product = await getProductDetails(user.cart.items);
    if (!getProductDetail) {
      throw new ApiError(
        "Invalid Product Purchase Request : Product Not Found",
        400
      );
    }
    // console.log("Product from user: " + product);
    const pre = getProductDetail.PreLiveState;
    const preSku = getProductDetail.Sku;
    //  console.log("make payment product pre  ..........",pre)
    //  console.log("make payment product sku ..........>>>>>>>>>",preSku)
    if (!pre)
      throw new ApiError("Unable to Purchase Product : Not in Live State", 400);
    const liveStock = [];
    //  console.log(preSku.length)
    // for (let i = 0; i < preSku.length; i++) {
    // console.log("in a loop")
    liveStock.push(await getStockOfProduct(preSku));
    // }
    //  console.log("stock status======?????",liveStock)
    //const stock = await getStockOfProduct(sku);
    if (liveStock.includes(false))
      throw new ApiError("Unable to Purchase Product: Out of Stock", 400);

    //console.log("Check error")

    //console.log("user ",user.cart.subtotal)
    //console.log("userCart",user.cart.items)
    //console.log("Product Name",product[0].Name)

    // console.log(itemsDetails(product))

    const amt = getProductDetail.finalPrice;
    const txnoid = "oid-" + Date.now();

    const prod = await mapByid([getProductDetail]);
    const product = [getProductDetail];
    // console.log("<=========>3333",prod)

    const txnAmt = Math.round(amt);
    const parameter = {
      transaction_details: {
        order_id: txnoid,
        gross_amount: txnAmt,
      },
      callbacks: {
        finish: process.env.BASE_URL + `/`,
        //?order_id=0311e755-3d86-4260-b80e-94b24394a20e&status_code=200&transaction_status=capture
      },
      credit_card: {
        secure: true,
      },
      item_details: [...itemsDetails([getProductDetail])],
      customer_details: {
        first_name: user.firstName,
        last_name: user?.lastName,
        email: user.email,
        phone: user?.phone,
        //   address
      },
    };
    // console.log('about to create first makePayment transaction');
    //
    snap
      .createTransaction(parameter)
      .then(async (transaction) => {
        const { token, redirect_url } = transaction;
        // console.log("make Payment Parameter...transactioon",parameter)
        // console.log('make Payment transaction response snap:  =======>', parameter);
        // console.log('transactionToken:', token);
        // console.log('make Payment Redirect to:', redirect_url);
        // await sendInVoice(user.email, user.firstName, prod, txnoid, new Date(), txnAmt )
        const txn = await saveTrasection(
          txnoid,
          txnAmt,
          `${user.firstName}${user.lastName ? " " + user.lastName : ""}`,
          user._id,
          //product.Name,
          //product.Sku,
          product,
          prod
        );
        //  console.log("Transaction ",txn.prod)

        // associate transaction with current user
        user.transactions.unshift(txn._id);
        // user.cart = {
        //   subtotal: 0,
        //   totalItem: 0,
        //   items: []
        // }
        await user.save();
        res.status(200).json({
          Error: false,
          PaymentToken: token,
          PaymentRedirectUrl: redirect_url,
          Transaction: txn,
        });
      })
      .catch((error) => next(error));
  } catch (error) {
    console.log("error from makePayment Outer ==>", error);
    next(error);
  }
};

module.exports = makePaymentforSingleProduct;
