// const checkoutProduct = require('./checkoutProduct');
const getProductKey = require("./getProductKey");
const makePayment = require("./makePayment");
const makePaymentforSingleProduct = require("./makePaymentforSingleProduct");
module.exports = {
  // checkoutProduct,
  makePayment,
  getProductKey,
  makePaymentforSingleProduct,
};
