const GenbaProduct = require('../../models/GenbaProdModel');

const getProductDetails = async (ProductId) => {
  //console.log("in function")
  if (!ProductId) return null;
  const records = await GenbaProduct.find({ '_id': { $in: ProductId } });
  return records
};

module.exports = getProductDetails;
