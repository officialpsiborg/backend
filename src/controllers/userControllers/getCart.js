const User = require('../../models/UserModel');

const getCart = async (req, res, next) => {
  try {
    const {select} = req.query;
    let selectedField = '';
    if (select) {
      selectedField = select
        .split(',')
        .filter(
          (item) =>
            !item.includes('KeySold')
        )
        .join(' ');
    }
    const user = await User.findOne({email: req.user.email})
      .select('cart')
      .populate('cart.items', selectedField);
    console.log("Cart???????",user)
    res.status(200).json({
      Error: false,
      Cart: user.cart,
    });
  } catch (error) {
    next(error);
  }
};

module.exports = getCart;
