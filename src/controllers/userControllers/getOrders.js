const User = require('../../models/UserModel');
const getProductDetails = require('../../helper/getProductDetails');
const TransactionModel = require('../../models/TransactionModel');
var ObjectId = require("mongodb").ObjectId;
//
const getOrders = async (req, res, next) => {
  try {
    const page = Number(req.query.page) || 1;
    const limit = Number(req.query.limit) || 10;
    let skips = (page - 1) * limit;
    

    // let user = await TransactionModel.find({user: "63ce683df08626d903c6a500", txn_status: "success" })
    let user = await TransactionModel.aggregate([
      {
        $match: {
          user: ObjectId(req.user._id), 
          txn_status: "success" 
        }
      },
      {
        $unwind: "$prod"
      },
      { $skip: skips },
      { $limit: limit },
    ])

    let totalDocs = await TransactionModel.aggregate([
      {
        $match: {
          user: ObjectId(req.user._id), //req.user._id
          txn_status: "success" 
        }
      },
      {
        $unwind: "$prod"
      }
    ])
    // let user = await User.findById("63ce683df08626d903c6a500")
    // .populate({
    //   path: 'transactions',
    //   model: 'Transaction',
    //   select: '_id',
    //   match: {
    //     txn_status: 'success',
    //     isDelivered: true,
    //     prod_key_status: 'delivered',
    //   },
    // });
    // console.log('\n==>', user.transactions);
    // totalDocs = user.transactions.length;
    // user = await user.populate({
    //   path: 'transactions',
    //   model: 'Transaction',
    //   select: 'txn_oid txn_id product prod_key txn_amt createdAt updatedAt',
    //   match: {
    //     txn_status: 'success',
    //     isDelivered: true,
    //     prod_key_status: 'delivered',
    //   },
    //   options: {
    //     // limit: limit,
    //     // skip: (page - 1) * limit,
    //     sort: {updatedAt: -1},
    //   },
    // })
   // console.log("??????",user)
    res.status(200).json({
      Error: false,
      TotalGames: totalDocs.length  || 0,
      Length: totalDocs.length || 0,
      Games: user,
    });
  } catch (error) {
    next(error);
  }
};
module.exports = getOrders;
