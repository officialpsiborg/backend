const mongoose = require('mongoose');
const User = require('../../models/UserModel');
const GenbaProduct = require('../../models/GenbaProdModel');
const ApiError = require('../../error-handler/ApiError');

const addToCart = async (req, res, next) => {
  try {
    const {select} = req.query;
    const gameId = req.params.gameId;
    let selectedField = '';
    if (select) {
      selectedField = select
        .split(',')
        .filter(
          (item) =>
            !item.includes('KeySold')
        )
        .join(' ');
    }
    if (!mongoose.Types.ObjectId.isValid(gameId))
      throw new ApiError('Invalid Product Id', 400);
    const prod = await GenbaProduct.findOne({_id: gameId});
    if (!prod) throw new ApiError('Invalid Product', 400);
    const user = await User.findOne({email: req.user.email});

    const gameInCart = user.cart.items.find((item) => item == gameId);
    // !gameInCart && user.cart.items.unshift(gameId);
    if (!gameInCart) {
      user.cart.items.unshift(gameId);
      // user.cart.totalItem += 1;
      user.populate('cart.items', selectedField);
      await user.save();
      let subtotal = 0;
      let totalItem = 0;
      user.cart.items.forEach((item) => {
        console.log("in lopp ",item.Srp,item.Price, item.finalPrice)
        if(item.onPromotion=== false){
          subtotal += item.finalPrice
          totalItem++;
        }else {
        subtotal += item.finalPrice;
        totalItem++;
        }
      });
      console.log("subtao; ",subtotal)
      user.cart.subtotal = Math.round(subtotal);
      user.cart.totalItem = totalItem;
      console.log("???????cart",user.cart.subtotal)
    } else {
      user.populate('cart.items', selectedField);
    }
    await user.save();
    res.status(200).json({
      Error: false,
      Message: 'Product is added to cart.',
      Cart: user.cart,  
    });
  } catch (error) {
    next(error);
  }
};
module.exports = addToCart;
