require("dotenv").config();
const axios = require("axios");
const ApplicationData = require("../../models/ApplicationDataModel");
const { getAccessToken } = require("../../services/genbaServices");
const getProductDetail = require("../../helper/getProductDetails");
const orderProductKey = async (txn) => {
  // these lines generate fake keys
  // to generate genuine keys comment out this and use below code instead
  // const str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890';
  // let key = '';
  // for (let i = 0; i < 16; ++i) {
  // if (i === 4 || i === 8 || i === 12) {
  //   key += '-';
  // }
  // key += str[Math.floor(Math.random() * str.length)];
  // }
  // console.log('New key generated ==>', key);
  // // return null;
  // return [key];
  //----------------------------------------------------------------
  //----------------------------------------------------------------
  // use below code to actual genuine product key
  // and comment out above code




  const { txn_oid, product, txn_amt, prod } = txn;
  try {
    const token = await getAccessToken();
    const pro = await getProductDetail(product);
    const url = process.env.GENBA_BASE_URL + "/orders";
    // const appData = await ApplicationData.find().limit(1);
    // const orderTax = appData.pop().orderTax;
    console.log("txn prod====>1 fafsafs", prod);

    const multiSku = pro.map((x) => {
      return {
        id: x.Sku,
        finalPrice: x.finalPrice,
        assId: x._id,
      };
    });

    console.log("res data=====>2 Sku", multiSku);
    console.log("mapper");
    const obj = prod.map((e) => {
      let temp = multiSku.find(
        (element) => element.assId.toString() == e.productId._id.toString()
      );
      if (temp.assId) {
        temp.unid = e.unid;
      }
      return temp;
    });
    console.log("obj cresated  -->",obj);

    const keys1 = [];
    // commented for loop for testing
    for (let i = 0; i <= obj.length - 1; i++) {
      // console.log("in loop for request",multiSku[i].id)
      // const body = {
      //   ClientTransactionID: obj[i].unid,
      //   Properties: {
      //     Sku: obj[i].id,
      //     SellingPrice: {
      //       NetAmount: obj[i].Srp - (obj[i].Srp * 10) / 100,
      //       GrossAmount: obj[i].Srp,
      //       CurrencyCode: "IDR",
      //     },
      //     CountryCode: "ID",
      //   },
      // };

      const body = {
        ClientTransactionID: obj[i].unid,
        Properties: {
          Sku: obj[i].id,
          SellingPrice: {
            NetAmount: obj[i].finalPrice,
            GrossAmount: obj[i].finalPrice,
            CurrencyCode: "IDR",
          },
          CountryCode: "ID",
        },
      };
      console.log("in loop for body",body)
      try {
        const res = await axios({
          method: "POST",
          url,
          headers: {
            Authorization: `Bearer ${token}`,
          },
          data: body,
        });
        // const data = res.data;
        console.log("key to be found ===>", res.data)
          if (res.data.State == 2 && res.data.Keys) {
            console.log("key was here",res.data.Keys)
            return res.data.Keys; 
         }
        // console.log("Genrating multiple keys",keys1)
        // console.log("Out of loop")
        // console.log("Key gentrator", keys1);
        // return keys1;
      
        
      } catch (error) {
        console.log("first Try catch ==>", error.message);
      }
    }
  } catch (error) {
    console.log("data Error yu ==>", error);
    console.log("Error Message ==>", error.message);
    console.log("\n====>1", error.data);
    console.log("\n====>2", error?.response?.data);
    return null;
  }
};


module.exports = orderProductKey;
