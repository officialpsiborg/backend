const User = require('../../models/UserModel');
// const mongoose = require('mongoose');
const ApiError = require('../../error-handler/ApiError');
// const GenbaProduct = require('../../models/GenbaProdModel');

const removeFromWishlist = async (req, res, next) => {
  try {
    const {select} = req.query;
    const gameId = req.params.gameId;
    let selectedField = '';
    if (select) {
      selectedField = select
        .split(',')
        .filter(
          (item) =>
            !item.includes('Wsp') &&
            !item.includes('Srp') &&
            !item.includes('KeySold')
        )
        .join(' ');
    }
    const user = await User.findOne({email: req.user.email});
    const i = user.wishlist.findIndex((wish) => wish._id == gameId);
    i != -1 && user.wishlist.splice(i, 1);
    user.populate('wishlist', selectedField);
    await user.save();
    res
      .status(200)
      .json({
        Error: false,
        Message: 'Product is removed from wishlist.',
        Wishlist: user.wishlist,
      });
  } catch (error) {
    next(error);
  }
};

module.exports = removeFromWishlist;
