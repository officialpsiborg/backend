const User = require('../../models/UserModel');
const mongoose = require('mongoose');
const ApiError = require('../../error-handler/ApiError');
const GenbaProduct = require('../../models/GenbaProdModel');

const addToWishlist = async (req, res, next) => {
  try {
    const {select} = req.query;
    const gameId = req.params.gameId;
    let selectedField = '';
    if (select) {
      selectedField = select
        .split(',')
        .filter(
          (item) =>
            !item.includes('Wsp') &&
            !item.includes('Srp') &&
            !item.includes('KeySold')
        )
        .join(' ');
    }
    if (!mongoose.Types.ObjectId.isValid(gameId))
       throw new ApiError('Invalid Product Id', 400);
    const prod = await GenbaProduct.findOne({_id: gameId});
    if (!prod) throw new ApiError('Invalid Product', 400);
    const user = await User.findOne({email: req.user.email});
    !user.wishlist.includes(gameId) && user.wishlist.unshift(gameId);
    user.populate('wishlist', selectedField);
    await user.save();
    res.status(200).json({
      Error: false,
      Message: 'Product is added to wishlist.',
      WishList: user.wishlist,
    });
  } catch (error) {
    next(error);
  }
};

module.exports = addToWishlist;
