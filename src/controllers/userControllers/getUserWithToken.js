require('dotenv').config();
const User = require('../../models/UserModel');
const bcrypt = require('bcrypt');
const genAccessToken = require('../userAuthControllers/genAccessToken');
const genRefreshToken = require('../userAuthControllers/genRefreshToken');
const ApiError = require('../../error-handler/ApiError');
//
const getUserWithToken = async (req, res, next) => {

  if (!req.user._id) {
    return res.status(404).json({msg: "user Not found"})
  }

  try {
    const user = await User.findById(req.user._id).select(
      '_id firstName lastName email phone gamesOwned verified role createdAt'
    );

    const accessToken = genAccessToken(
        {
          _id: user._id,
          email: user.email,
          role: user.role,
        },
        process.env.JWT_EXPIRE
      );
      const refreshToken = await genRefreshToken(
        {
          _id: user._id,
          email: user.email,
          role: user.role,
        },
        process.env.JWT_REFRESH_EXPIRE
      );
      user.refreshToken = refreshToken; //for later use of http only cookie
      await user.save();
      return res.status(200).json({
        Error: false,
        Message: 'Successfully logged in.',
        AccessToken: accessToken,
        AccessTokenExpiry: process.env.JWT_EXPIRE,
        RefreshToken: refreshToken,
        RefreshTokenExpiry: process.env.JWT_REFRESH_EXPIRE,
        User: {
          FirstName: user.firstName,
          LastName: user?.lastName,
          Email: user.email,
          Role: user.role,
          Phone: user?.phone,
          Verified: user.verified,
          CreatedAt: user.createdAt,
        },
      });
  } catch (error) {
    // next(error);
    console.log("error get UserToken ==>", error.message)
    return res.status(500).json({msg: error.message})
  }
};
module.exports = getUserWithToken;
