const mongoose = require('mongoose');
const User = require('../../models/UserModel');
const ApiError = require('../../error-handler/ApiError');

const removeFromCart = async (req, res, next) => {
  try {
    const {select} = req.query;
    const gameId = req.params.gameId;
    let selectedField = '';
    if (select) {
      selectedField = select
        .split(',')
        .filter(
          (item) =>
            !item.includes('KeySold')
        )
        .join(' ');
    }
    if (!mongoose.Types.ObjectId.isValid(gameId))
      throw new ApiError('Invalid Product Id', 400);
    const user = await User.findOne({email: req.user.email});

    const i = user.cart.items.findIndex((item) => item == gameId);
    if (i !== -1) {
      user.cart.items.splice(i, 1);
      user.populate('cart.items', selectedField);
      await user.save();
      let subtotal = 0;
      let totalItem = 0;
      user.cart.items.forEach((item) => {
        if(item.onPromotion===false){
        subtotal += item.Price;
        totalItem++;
        }else {
          subtotal += item.onPromotionPrice;
          totalItem++
        }
      });
      user.cart.subtotal = subtotal;
      user.cart.totalItem = totalItem;
    } else {
      user.populate('cart.items', selectedField);
    }
    await user.save();
    res.status(200).json({
      Error: false,
      Message: 'Product is removed from cart.',
      Cart: user.cart,
    });
  } catch (error) {
    next(error);
  }
};
module.exports = removeFromCart;
