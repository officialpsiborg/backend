const ApiError = require('../../error-handler/ApiError');
const User = require('../../models/UserModel');

//
const getTransactions = async (req, res, next) => {
  try {
    const reqUser = req.user;

    // const {userId, userEmail} = req.body;
    // const findQuery = {};
    // if (userId) findQuery._id = userId;
    // if (userEmail) findQuery.email = userEmail;
    // if (!userId && !userEmail) throw new ApiError('User Not Found',404);

    const page = req.query.page || 1;
    const limit = req.query.limit || 10;

    const user = await User.findOne({email: reqUser.email})
      .select('-_id transactions')
      .populate({
        path: 'transactions',
        model: 'Transaction',
        // select:'',
        options: {
          limit: limit,
          skip: (page - 1) * limit,
          sort: {updatedAt: -1},
        },
      });
    if (!user) throw new ApiError('User Not Found', 404);
    return res.status(200).json({
      Error: false,
      Transactions: user.transactions,
    });
  } catch (error) {
    next(error);
  }
};

module.exports = getTransactions;
