const User = require('../../models/UserModel');

//
const getUserDetails = async (req, res, next) => {
  try {
    const user = await User.findOne({
      email: req.user.email,
      role: req.user.role,
    }).select(
      '_id firstName lastName email phone gamesOwned verified role createdAt'
    );
    res.status(200).json({Error: false, User: user});
  } catch (error) {
    next(error);
  }
};
module.exports = getUserDetails;
