const getTransactions = require('./getTransactions');
const updateUserDetails = require('./updateUserDetails');
const getUserDetails = require('./getUserDetails');
const getOrders = require('./getOrders');
const changePassword = require('./changePassword');
const addToWishlist = require('./addToWishlist');
const getWishlist = require('./getWishlist.');
const removeFromWishlist = require('./removeFromWishlist');
const addToCart = require('./addToCart');
const removeFromCart = require('./removeFromCart');
const getCart = require('./getCart');
const cartCheckout = require('./cartCheckout');
const getUserWithToken = require('./getUserWithToken');
module.exports = {
  getUserDetails,
  updateUserDetails,
  getTransactions,
  getOrders,
  changePassword,
  addToWishlist,
  getWishlist,
  removeFromWishlist,
  addToCart,
  removeFromCart,
  getCart,
  cartCheckout,
  getUserWithToken
};