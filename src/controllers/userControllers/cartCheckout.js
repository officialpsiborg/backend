require('dotenv').config();
// const {v4: uuid} = require('uuid');
const midtransClient = require('midtrans-client');
// const getProductDetails = require('./getProductDetails');
const ApiError = require('../../error-handler/ApiError');
const {getStockOfProduct} = require('../../services/genbaServices');
const getUserDetails = require('./getUserDetails');
// const saveTrasection = require('./saveTransection');
const User = require('../../models/UserModel');
const sendInVoice = require('../buyProductControllers/sendInvoice');

const snap = new midtransClient.Snap({
  isProduction: false,
  serverKey: process.env.PAYMENT_SERVER_KEY,
});

const cartCheckout = async (req, res, next) => {
  try {
    console.log("CART CHECKOUT ==>")
    const user = await User.findOne({email: req.user.email});
    // console.log(user.cart.items.length);
    if (user.cart.items.length === 0)
      throw new ApiError('Unable to checkout. Cart is empty.', 400);

    user.populate('cart.items', 'Sku Name Price Discount Stock PreLiveState');
    await user.save();

    // user.cart.items.forEach((item) => {
    //   if (!item.Stock && item.PreLiveState != 1)
    //     throw new ApiError('Unable to checkout. Product is not in stock', 400);
    // });

    // const stock = await checkItemStock(user.cart.items); //in genba
    // if (!stock)
    //   throw new ApiError(`Unable to checkout. Product is out of Stock`, 400);

    const txnoid = 'oid-' + Date.now();
    const txnAmt = Math.round(user.cart.subtotal);
    const parameter = {
      transaction_details: {
        order_id: txnoid,
        gross_amount: txnAmt,
      },
      callbacks: {
        finish: process.env.BASE_URL + `/`,
      },
      credit_card: {
        secure: true,
      },
      item_details: [...itemsDetails(user.cart.items)],
      customer_details: {
        first_name: user.firstName,
        last_name: user?.lastName,
        email: user.email,
        phone: user?.phone,
      },
    };
    //====================
    snap.createTransaction(parameter).then(async (transaction) => {
      const {token, redirect_url} = transaction;
      console.log('res snap ===> ', transaction);
      // sendInVoice()
      // await sendInVoice(user.email, user.firstName, prod, txnoid, new Date(), txnAmt )
      
      res.status(200).json({
        Error: false,
        PaymentToken: token,
        PaymentRedirectUrl: redirect_url,
      });
    });
    //========================
  } catch (error) {
    next(error);
  }
};

async function checkItemStock(items) {
  for (let i = 0; i < items.length; i++) {
    const stock = await getStockOfProduct(items[i].Sku);
    if (!stock) return false;
  }
  return true;
}

function itemsDetails(items) {
  return items.map((item) => {
    return {
      id: item._id,
      name: item.Name,
      sku: item.Sku,
      quantity: 1,
      price: item.Price,
    };
  });
}
module.exports = cartCheckout;
