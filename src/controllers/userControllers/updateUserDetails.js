const User = require('../../models/UserModel');

const updateUserDetails = async (req, res, next) => {
  try {
    let {firstName, lastName, phone} = req.body;
    const reqUser = req.user;
    const updateData = {};
    if (firstName) updateData.firstName = firstName.trim();
    if (lastName) updateData.lastName = lastName.trim();
    if (phone) updateData.phone = phone.trim();
    const updatedUser = await User.findOneAndUpdate(
      {
        email: reqUser.email,
        role: reqUser.role,
      },
      updateData,
      {new: true}
    );

    res.status(201).json({
      Error: false,
      Message: 'User updated successfully',
      User: {
        FirstName: updatedUser.firstName,
        LastName: updatedUser.lastName,
        Email: updatedUser.email,
        Phone: updatedUser.phone,
        Role: updatedUser.role,
        Verified: updatedUser.verified,
      },
    });
    console.log(reqUser);
  } catch (error) {
    next(error);
  }
};

module.exports = updateUserDetails;
