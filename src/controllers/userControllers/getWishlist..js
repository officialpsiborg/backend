const User = require('../../models/UserModel');

const getWishlist = async (req, res, next) => {
  try {
    const {select} = req.query;
    let selectedField = '';
    if (select) {
      selectedField = select
        .split(',')
        .filter(
          (item) =>
            !item.includes('Wsp') &&
            !item.includes('Srp') &&
            !item.includes('KeySold')
        )
        .join(' ');
    }
    const user = await User.findOne({email: req.user.email})
      .select('wishlist')
      .populate('wishlist', selectedField);

    res.status(200).json({
      Error: false,
      Wishlist: user.wishlist,
    });
  } catch (error) {
    next(error);
  }
};

module.exports = getWishlist;
