const UserModel = require('../../models/UserModel');
const bcrypt = require('bcrypt');
const ApiError = require('../../error-handler/ApiError');
const changePassword = async (req, res, next) => {
  try {
    const {password, newPassword} = req.body;
    if (!password || !newPassword) {
      throw new ApiError('Current Password and New Password are required', 400);
    }
    const user = await UserModel.findOne({email: req.user.email});
    const verified = await bcrypt.compare(password, user.password);
    if (!verified) {
      throw new ApiError(`Invalid current password`, 403);
    }
    const samePassword = await bcrypt.compare(newPassword, user.password);
    if (samePassword) {
      throw new ApiError(
        'New password should not be the current password',
        400
      );
    }
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(newPassword, salt);
    user.password = hashedPassword;
    await user.save();

    res.status(200).json({
      Error: false,
      Message: 'Password Successfully Changed',
    });
  } catch (error) {
    next(error);
  }
};

module.exports = changePassword;
