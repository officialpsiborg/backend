const GenbaProduct = require('../../models/GenbaProdModel');

const getSimilarProducts = async (req, res, next) => {
  try {
    const {genres} = req.query;
    const findQuery = {};
    let genresArray = [];
    if (genres) {
      genresArray = genres.split(',');
      findQuery.Genres = {$in: genresArray};
    }
  //  console.log(findQuery);
    const result = await GenbaProduct.find(findQuery)
      .limit(6)
      .select('-KeySold');
     const temp =  result.filter(x=>x.Srp) 
    //  console.log("Similart?????",temp)
    res
      .status(200)
      .json({Error: false, Products: result, Length: result.length});
  } catch (error) {
    next(error);
  }
};

module.exports = getSimilarProducts;
