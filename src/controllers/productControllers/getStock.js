const GenbaProduct = require('../../models/GenbaProdModel');
const {getStockOfProduct} = require('../../services/genbaServices');

const getStock = async (req, res, next) => {
  try {
    const Sku = req.params.Sku;
    //get price and live state then stock
    const product = await GenbaProduct.findOne({Sku}).select(
      'Sku Name Price PreLiveState'
    );
    // const product = await getProductDetails(sku, 'Sku Name Price PreLiveState');
    if (!product) {
      return res.status(200).json({
        Error: false,
        Message: 'Product is out of stock [Product is not found]',
        InStock: false,
      });
    }

    if (product.PreLiveState !== 1) {
      return res.status(400).json({
        Error: true,
        Message: 'Product is Out of Stock. [Product is not purchasable]',
        InStock: false,
      });
    }

    let stock = await getStockOfProduct(product.Sku);

    if (!stock) {
      return res.status(400).json({
        Error: false,
        Message: 'Product is out of stock',
        InStock: false,
      });
    }
    if (stock) {
      return res.status(200).json({
        Error: false,
        Message: 'Product is in stock',
        InStock: true,
      });
    }
  } catch (error) {
    next(error);
  }
};
module.exports = getStock;

/*
const ApiError = require('../../error-handler/ApiError');
const {getStockOfProduct} = require('../../services/genbaServices');
const getProductDetails = require('./getProductDetails');

const checkoutProduct = async (req, res, next) => {
  try {
    const sku = req.params.sku;
    //get price and live state then stock
    let stock = false;
    const product = await getProductDetails(sku, 'Sku Name Price PreLiveState');
    if (
      product &&
      product.PreLiveState == 1 &&
      (await getStockOfProduct(product.Sku))
    ) {
      stock = true;
      return res.status(200).json({
        Error: false,
        Message: 'Product is in stock',
        Product: {
          Name: product.Name,
          Sku: product.Sku,
          BuyingPrice: Math.round(product.Price),
          LiveState: product.PreLiveState,
          InStock: true,
        },
        User: {
          Email: req.user.email,
        },
      });
    }
    if (product && product.PreLiveState == 1 && !stock) {
      return res.status(400).json({
        Error: true,
        Message: 'Product is out of stock',
        Product: {
          Name: product.Name,
          Sku: product.Sku,
          BuyingPrice: Math.round(product.Price),
          LiveState: product.PreLiveState,
          InStock: false,
        },
        User: {
          Email: req.user.email,
        },
      });
    }
    if (product && product.PreLiveState != 1) {
      return res.status(400).json({
        Error: true,
        Message: 'Product is not purchasable',
        Product: {
          Name: product.Name,
          Sku: product.Sku,
          BuyingPrice: Math.round(product.Price),
          LiveState: product.PreLiveState,
          InStock: false,
        },
        User: {
          Email: req.user.email,
        },
      });
    }
    if (!product) {
      throw new ApiError('Invalid Product Purchase Request');
    }
  } catch (error) {
    next(error);
  }
};

module.exports = checkoutProduct;
*/
