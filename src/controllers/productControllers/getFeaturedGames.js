const GameCategory = require('../../models/GameCategoryModel');

const getFeaturedGames = async (req, res, next) => {
  try {
    const select = req.query.select;
    let selectedFields = '';
    if (select) {
      selectedFields = select.split(',').join(' ');
    }
    selectedFields += '-KeySold';
    const result = await GameCategory.findOne({
      Order: 1,
    }).populate('Games', selectedFields);
    if (!result) {
      return res.status(404).json({
        Error: true,
        Length: 0,
        FeaturedGames: [],
      });
    }

    // console.log("feature game",result)
    return res.status(200).json({
      Error: false,
      Length: result.Games.length,
      FeaturedGames: result.Games,
    });
  } catch (error) {
    next(error);
  }
};

module.exports = getFeaturedGames;
