const getCategory = require('./getCategory');
const getFeaturedGames = require('./getFeaturedGames');
const getProducts = require('./getProducts');
const getSimilarProducts = require('./getSimilarProducts');
const getStock = require('./getStock');
const getBundleGames = require('./getBundleGames');
const getDetailedGame = require('./getDetailedGame');
const getUpcomingGames = require('./getUpcomingGames');
const getGameFilter = require("./getGameFilter");
const updateProductGame = require("./updateProduct")
module.exports = {
  getProducts,
  getCategory,
  getStock,
  getFeaturedGames,
  getSimilarProducts,
  getBundleGames,
  getDetailedGame,
  getUpcomingGames,
  getGameFilter,
  updateProductGame
};
