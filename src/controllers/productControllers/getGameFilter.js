const GenbaProduct = require('../../models/GenbaProdModel');

const getGameFilter = async (req, res, next) => {
    const {genre, publisher, keyProvider, platform} = req.query;
    let query 
    if (genre) query = "Genres";
    if (publisher) query = "Publisher";
    if (keyProvider) query = "KeyProvider";
    if (platform) query = "Platform";

    try{
        let result = await GenbaProduct.distinct(query)
        if (result) {
            return res.status(200).json({Error: false, Length: result.length, msg: result})
        }
    } catch (error) {
        next(error)
    }
}



module.exports = getGameFilter;