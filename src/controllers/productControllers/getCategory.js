const ApiError = require('../../error-handler/ApiError');
const GameCategory = require('../../models/GameCategoryModel');
const mongoose = require('mongoose');
const getCategory = async (req, res, next) => {
  try {
    let {id, _id, select} = req.query;
    if (id) _id = id;
    // const findQuery = {Name: {$ne: 'FeaturedGames'}}; // to exclude featured games category
    const findQuery = {};
    if (_id) findQuery._id = _id;
    if (_id && !mongoose.Types.ObjectId.isValid(_id)) {
      throw new ApiError('Category id is invalid');
    }

    // if (query) findQuery.Name = {$regex: `${query}`, $options: 'i'};

    // if (select) {
    //   selectedField += select.split(',').join(' ');
    // }
    // selectedField += ' -Wsp -Srp -KeySold';

    let selectedField = [];
    if (select) {
      selectedField = select
        .split(',')
        .filter(  
          (item) => 
            !item.includes('Wsp') &&
            !item.includes('Srp') &&
            !item.includes('KeySold')
        )
        .join(' ');
    }

    const categories = await GameCategory.find(findQuery)
      .select('-__v')
      .sort({Order: 1})
      .populate('Games', selectedField);
    res.status(200).json({
      Error: false,
      Length: categories.length,
      Category: categories,
    });
  } catch (err) {
    next(err);
  }
};

module.exports = getCategory;
