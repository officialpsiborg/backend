const mongoose = require('mongoose');
const GenbaProduct = require('../../models/GenbaProdModel');
//user
const getProducts = async (req, res, next) => {
  try {
    const {
      sku,
      query,
      platform,
      KeyProvider,
      genres,
      select,
      publisher,
      // sortprice = 'des',
    } = req.query;
    let {id, _id, pricefrom, priceto,  latest} = req.query;
    const findQuery = {};
    let genresArray = [];
    let selectedField = [];
    if (id) _id = id;
    if (_id) findQuery._id = _id;
    if (sku) findQuery.Sku = sku;
    if (platform) findQuery.Platform =  {$in: platform.split(',')};
    if (KeyProvider) findQuery.KeyProvider = KeyProvider;
    if (publisher) findQuery.Publisher = {$in: publisher.split(',')};
    // if (query) findQuery.Name = {$regex: `${query}`, $options: 'i'};
    if (query) {
      findQuery.$or = [
        {Name: {$regex: `${query}`, $options: 'i'}},
        {Publisher: {$regex: `${query}`, $options: 'i'}},
        {Developer: {$regex: `${query}`, $options: 'i'}},
      ];
    }

    // if (priceto) {
    //   priceto = parseFloat(priceto);
    //   pricefrom = pricefrom ? parseFloat(priceto) : 0;
    //   findQuery.Price = {$gt: pricefrom, $lte: priceto};
    // }

    if (pricefrom && priceto) {
      findQuery.finalPrice = {
        $gte: parseFloat(pricefrom),
        $lte: parseFloat(priceto),
      };
    }
    if (pricefrom && !priceto) findQuery.finalPrice = {$gte: parseFloat(pricefrom)};
    if (!pricefrom && priceto) findQuery.finalPrice = {$lte: parseFloat(priceto)};

    if (genres) {
      genresArray = genres.split(',');
      findQuery.Genres = {$in: genresArray};
    }

    if (latest) {
      findQuery.ReleaseDate = {
        $lte: new Date().toISOString().split('T')[0]
      }
    }

    if (select) {
      // selectedField = select.split(',').join(' ');
      selectedField = select
        .split(',')
        .filter(
          (item) =>
            !item.includes('KeySold')
        )
        .join(' ');
    }
   findQuery.Disabled = false
   findQuery.Srp = {$exists:true}
  //  findQuery.PreLiveState = 1;

    const page = req.query.page || 1;
    const limit = req.query.limit || 10;
    if (_id && !mongoose.Types.ObjectId.isValid(_id)) {
      return res.status(200).json({Error: false, Length: 0, Products: []});
    }

    console.log("findQuery ==>", findQuery)
    const result = await GenbaProduct.find(findQuery).sort({"ReleaseDate": -1})
      .skip((page - 1) * limit)
      .limit(limit)
      .select(selectedField);


    const totalPage = await GenbaProduct.countDocuments(findQuery)
     // console.log(result)
   //   const temp = result.filter(x=>x.Srp )
      //console.log("?????",temp)
    return res
      .status(200)
      .json({Error: false, Length: totalPage, Products: result});
  } catch (err) {
    next(err);
  }
};

module.exports = getProducts;
