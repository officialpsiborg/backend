const ApiError = require("../../error-handler/ApiError")

const GenbaProdModel = require("../../models/GenbaProdModel")


const getProductDetailss = async (ProductId) => {
    console.log("get ProductDetailsssssssssss",ProductId)


    if (!ProductId) return null;
        const records = await GenbaProdModel.find({'Sku':{$in:ProductId}});
    //console.log("get records",records)
    return records
  };
  


const getBundleGames = async(req,res)=>{
    try{
        const result = await GenbaProdModel.find({_id:req.params.id})
        // console.log("result ==",result)

        if(!result) return new ApiError("ID is invalid in req.params")
       // console.log("bundle product sku ")
  
        const IDS = [...result[0].BundleProductsSku]
       // console.log("IDS",typeof IDS)
        const products =  await getProductDetailss(IDS)
      //  console.log("PRoducts in the bundle",products)
        if(!products) return new ApiError("Product not found")
        res.status(200).json({
            message:"Bundle product fetch successfully",
            length:products.length,
            data:products
        })        
    }catch(e){
        console.log("++++++++++++++",e)

    }

}
module.exports = getBundleGames