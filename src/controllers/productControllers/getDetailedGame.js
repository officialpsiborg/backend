const ApiError = require("../../error-handler/ApiError")
const GenbaProdModel = require("../../models/GenbaProdModel")


const getDetailedGame = async (req,res)=>{
    try{
        const game = await GenbaProdModel.findOne({_id:req.params.id})
        // console.log("++++game",game)
        if(!game) return new ApiError("Details of game not found")
        res.status(200).json({
            message:"Deatils of game has been found",
            game:game
        })
    }catch(e){
        console.log("=================>",e)
    }
}
module.exports = getDetailedGame