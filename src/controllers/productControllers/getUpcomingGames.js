const GenbaProduct = require("../../models/GenbaProdModel");

const getUpcomingGames = async (req, res, next) => {
    let date = req.query.today
    if (!date) date = new Date().toISOString().split('T')[0]
  try {
    const result = await GenbaProduct.find({
      ReleaseDate: {
        $gte: date
      },
    });
    if (!result) {
      return res.status(404).json({
        Error: true,
        Length: 0,
        Games: [],
      });
    }

    return res.status(200).json({
      Error: false,
      Length: result.length,
      Games: result,
    });
  } catch (error) {
    next(error);
  }
};

module.exports = getUpcomingGames;
