const GenbaProdModel = require('../../models/GenbaProdModel')
const Promotions = require('../../models/Promotions')

const getPromotions = async (req,res)=>{

    const {query, sortPricePattern, sortDiscountPattern, sortNamePattern, sortStartDate, sortEndDate, onPromotionSrp} = req.query;
    try{
        let findQuery = {}
        let sortQuery = {}
        findQuery.onPromotion = true
        if (query) {
            findQuery.$or = [
              {Name: {$regex: `${query}`, $options: 'i'}},
              {Publisher: {$regex: `${query}`, $options: 'i'}},
              {Developer: {$regex: `${query}`, $options: 'i'}},
            ];
          }


          if (sortPricePattern && sortPricePattern !== "undefined") sortQuery.finalPrice = Number(sortPricePattern) ? 1 : -1
          if (sortDiscountPattern && sortDiscountPattern !== "undefined") sortQuery.finalDiscount = Number(sortDiscountPattern) ? 1 : -1
          if (sortNamePattern && sortNamePattern !== "undefined") sortQuery.Name = Number(sortNamePattern) ? 1 : -1
         
          if (sortStartDate && sortStartDate !== "undefined") sortQuery.onPromotionFrom = Number(sortStartDate) ? 1 : -1
          if (sortEndDate && sortEndDate !== "undefined") sortQuery.onPromotionTo = Number(sortEndDate) ? 1 : -1
          if (onPromotionSrp && onPromotionSrp !== "undefined") sortQuery.onPromotionSrp = Number(onPromotionSrp) ? 1 : -1
         
        //   console.log("GET PROMOTIONS Query 0==>", sortQuery);
       //  const result =  await Promotions.aggregate([
          //  {
         //   $project:{
          //      PromotionItems:{
           //         $filter:{
             //           input:"$PromotionItems",
             //           as:"item",
               //         cond:{$eq:["$$item.CurrencyCode","IDR"]}}}}}])

               const page = req.query.page || 1;
                const limit = req.query.limit || 10;
        const result =  await GenbaProdModel.find(findQuery).sort(sortQuery).skip((page - 1) * limit)
        .limit(limit)

        const totalDocs = await GenbaProdModel.countDocuments(findQuery)
        // const temp = result.map((e)=>e.PromotionItems.filter(e=>e.CurrencyCode==="IDR"))
       // console.log("ttempppp=???????????????????",temp)
        if(!result){
            throw new Error("No Promotions found")
        }

        res.status(200).json({
            message:"FOUND ALL THE PROMOTION",
            length:totalDocs || 0,
            data:result,
            
        })

    }catch(e){
        console.log("Error in the Promotions",e)
    }
}
module.exports = getPromotions