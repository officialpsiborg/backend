const ApiError = require('../../error-handler/ApiError')
const GenbaProdModel = require('../../models/GenbaProdModel')

const featurePromotionalGames = async (req,res)=>{
    try{
        const result =  await GenbaProdModel.find({onPromotion:true}).limit(5)
        if(!result) return new ApiError("No result has been found")
        res.status(200).json({
            message:"featurePromotionalGames has been found",
            length:result.length,
            data:result,
           
        })
    }catch(e){
        console.log("Error in featurePromotionalGames ",e)
    }
}

module.exports = featurePromotionalGames