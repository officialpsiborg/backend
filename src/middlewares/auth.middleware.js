const verifyAccessToken = require('../controllers/userAuthControllers/verifyAccessToken');
const ApiError = require('../error-handler/ApiError');

const authenticateUser = async (req, res, next) => {
  try {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];
    // console.log('tokenn', token);
    const user = verifyAccessToken(token);
    if (user.role === 'user' || user.role === 'admin') {
      req.user = user;
      console.log('access token authenticated');
      return next();
    }
    throw new ApiError('Access forbidden', 401);
  } catch (err) {
    next(err);
  }
};

const authenticateAdmin = async (req, res, next) => {
  try {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];
    // console.log('tokenn', token);
    const user = verifyAccessToken(token);
    if (user.role === 'admin') {
      req.user = user;
      console.log('access token authenticated');
      return next();
    }
    throw new ApiError('Access Forbidden : Only Allowed for Admin', 401);
  } catch (err) {
    next(err);
  }
};

module.exports = {
  authenticateUser,
  authenticateAdmin,
};
