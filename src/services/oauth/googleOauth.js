
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const User = require("../../models/UserModel")

module.exports = (passport) => {
    passport.use(new GoogleStrategy({
    clientID: process.env.googleClientId,
    clientSecret: process.env.googleClientSecret,
    callbackURL: "https://api.mckey.io/auth/google/callback",
    passReqToCallback : true
    },
    async (request, accessToken, refreshToken, profile, done) => {
    try {
        console.log("profile from google Auth >>", profile)
    let existingUser = await User.findOne({ $or : [{'google.id': profile.id }, {email: profile.emails[0]?.value}]});
    // if user exists return the user
    if (existingUser) {
        console.log("== existingUser return from func() ==", existingUser)
    return done(null, existingUser);
    }
    // if user does not exist create a new user
    console.log('Creating new user...');
    const newUser = new User({
    method: 'google',
    google: {
    id: profile.id,
    name: profile.displayName,
    email: profile.emails[0]?.value
    },
    email: profile.emails[0]?.value,
    firstName: profile.displayName.split(' ')[0],
    lastName: profile.displayName.split(' ')[1],
    password: profile.id
    });
    await newUser.save();
    return done(null, newUser);
    } catch (error) {
        console.log("error from Oauth google =>", error.message)
    return done(error, false)
    }
    }
    ));
    }