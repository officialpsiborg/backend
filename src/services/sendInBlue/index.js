const Sib = require("sib-api-v3-sdk");
const client = Sib.ApiClient.instance;
const apiKey = client.authentications["api-key"];
const fs = require("fs");
const ejs = require("ejs");
apiKey.apiKey = process.env.API_KEY;
const path = require("path");
module.exports = async function sendBlueMail(emailConfig) {
  // console.log("=======> sendMail", emailConfig)
  let source;
  let htmlTemp;
  if (emailConfig.type === "Registration") {
    let filePath = path.join(process.cwd(), "./views/Registration/index.ejs");
     console.log("FILE PATH ==> ", filePath)
    source = fs.readFileSync(filePath, 'utf-8').toString();
    htmlTemp = ejs.render(source, {name: emailConfig.name, verificationLink: emailConfig.verificationLink})
  }

  if (emailConfig.type === "OrderInvoice") {
    let filePath = path.join(process.cwd(), "./views/OrderInvoice/index.ejs");
     console.log("OrderInvoice FILE PATH ==> ", filePath)
    source = fs.readFileSync(filePath, 'utf-8').toString();
    htmlTemp = ejs.render(source, {name: emailConfig.name, games: emailConfig.games, orderId: emailConfig.orderId, time: emailConfig.time, totalAmount: emailConfig.totalAmount})
  }
  return new Promise((resolve, reject) => {
    const tranEmailApi = new Sib.TransactionalEmailsApi();
    const sender = {
      email: emailConfig.senderEmail,
      name: "no-reply",
    };
    const receivers = [
      {
        email: emailConfig.recieverEmail,
      },
    ];
    // console.log("htmlContent", htmlTemp || emailConfig.body)
    tranEmailApi
      .sendTransacEmail({
        sender,
        to: receivers,
        subject: emailConfig.subject,
        htmlContent: htmlTemp || emailConfig.body,
      })
      .then((info) => {
        console.log(" Sending mail ... ==> ", info);
        return resolve(true);
      })
      .catch((error) => {
        console.log(" Error in sending mail from sendinBlue ==> ", error.status);
        return reject(false);
      });
  });
};

//let emailConfig = {
////  senderEmail: "no-reply@postmyad.ai",
// recieverEmail: "rahul.kumar@psiborg.in",
 /// subject: "Verify email for user",
//  body:
///    "<h1><b>Thanks for choosing us.</b></h1>" +
 //   "<p>Please click the below link to verify your email for your POSTMYAD Account.</p>"+
 //   `<a href="http://postmyad.ai:2000/">Verify</a>`,
//};
//sendBlueMail(emailConfig)