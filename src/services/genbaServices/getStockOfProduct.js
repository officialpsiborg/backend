require('dotenv').config();
const axios = require('axios');
const getAccessToken = require('./getAccessToken');

const getStockOfProduct = async (sku) => {
  try {
    const token = await getAccessToken();
    const url = process.env.GENBA_BASE_URL + `/productstock/${sku}`;
    const res = await axios({
      method: 'GET',
      url,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    if (res.data.StockStatus === 1) {
      // console.log('Product is in stock');
      return true;
    }
    return false;
  } catch (error) {
    console.error('res status', error?.response?.status);
    console.error('res data', error?.response?.data);
    return false;
  }
};

module.exports = getStockOfProduct;
