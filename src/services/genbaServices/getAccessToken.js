const axios = require('axios');

const genbaAccessToken = {
  expiry: null,
  token: null,
};

const updateGenbaAccessToken = (accessToken) => {
  genbaAccessToken.token = accessToken;
  genbaAccessToken.expiry = Math.floor(Date.now() / 1000 + 1 * 60 * 60 - 10); // 3590 seconds

  return genbaAccessToken.token;
};

const getAccessToken = async () => {
 
  if (genbaAccessToken.expiry > Math.floor(Date.now() / 1000)) {
    return genbaAccessToken.token;
  }

  const getClientAssertion = require('./getClientAssertion');

  let bodyDetails = {
    client_id:
      'https://aad.genbadigital.io/339b90a8-9202-4d3b-8699-f3b0b182299f',
    client_assertion_type:
      'urn:ietf:params:oauth:client-assertion-type:jwt-bearer',
    resource: '3d9998dc-c049-4108-b664-f6cc883f77e5',
    grant_type: 'client_credentials',
    client_assertion: getClientAssertion(),
  };

  let formBody = [];
  for (let property in bodyDetails) {
    let encodedKey = encodeURIComponent(property);
    let encodedValue = encodeURIComponent(bodyDetails[property]);
    formBody.push(`${encodedKey}=${encodedValue}`);
  }
  // console.log('In Access token ========>>>>>>>>>>>2', formBody);
  formBody = formBody.join('&');
  const uri =
    'https://login.microsoftonline.com/aad.genbadigital.io/oauth2/token';
 
  try {
    const res = await axios({
      method: 'POST',
      url: uri,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
      },
      data: formBody,
    });
    // console.log("token from getAccessToken ==>", res.data)
    return updateGenbaAccessToken(res.data.access_token);
  } catch (err) {
    console.log('error genbaServices', err.message);
     throw err;
  }
};

module.exports = getAccessToken;
