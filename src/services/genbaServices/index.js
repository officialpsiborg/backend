const getAccessToken = require('./getAccessToken');
const getStockOfProduct = require('./getStockOfProduct');
const requestApprovalForProduct = require('./requestApprovalForProduct');
const requestAllApprovalForProduct = require('./requestAllApprovalForProduct')
// const getClientAssertion = require('./getClientAssertion');
module.exports = {
  // getClientAssertion,
  getAccessToken,
  getStockOfProduct,
  requestApprovalForProduct,
  requestAllApprovalForProduct,
};
