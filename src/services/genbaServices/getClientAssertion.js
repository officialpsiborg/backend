const jwt = require('jsonwebtoken');
const fs = require('fs');
const path = require('path');

const clientAssertion = {
  token: null,
  expiry: null,
};
const updateClientAssertion = (token) => {
  clientAssertion.token = token;
  clientAssertion.expiry = Math.floor(Date.now() / 1000 + 12 * 60 * 60 - 10);
  // console.log('New Client Assertion Returned');
  return clientAssertion.token;
};

const getClientAssertion = () => {
  try {
    if (clientAssertion.expiry > Math.floor(Date.now() / 1000)) {
      // console.log('Old Client Assertion Returned');
      return clientAssertion.token;
    }
    const payload = {
      aud: 'https://login.microsoftonline.com/aad.genbadigital.io/oauth2/token',
      exp: Math.floor(Date.now() / 1000 + 12 * 60 * 60), //12h from now
      iss: 'https://aad.genbadigital.io/339b90a8-9202-4d3b-8699-f3b0b182299f',
      sub: 'https://aad.genbadigital.io/339b90a8-9202-4d3b-8699-f3b0b182299f',
      jti: '22b3bb26-e046-42df-9c96-65dbd72c1c81',
    };
    const signOtions = {
      algorithm: 'RS256',
      header: {
        alg: 'RS256',
        typ: 'JWT',
        x5t: 'ChKn4Gr9lj5oQKdNFsJT9hO3kmg=', //change when certificate thumbprint is changed and please make sure that  to change value  of hexadecimal to base64
      },
    };
    const privateKey = fs.readFileSync(
      path.resolve(__dirname, '../../certificate/private.key'),
      'utf-8'
    );

    const token = jwt.sign(payload, privateKey, signOtions);
   // console.log("client assertion token",token)
    return updateClientAssertion(token);
  } catch (error) {
    console.log('error in clientAssertion> ', error.message);
  }
};

module.exports = getClientAssertion;
