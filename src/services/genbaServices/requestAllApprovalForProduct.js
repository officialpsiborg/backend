const GenbaProdModel = require("../../models/GenbaProdModel")
const getAccessToken = require('./getAccessToken');
const { updatePreLiveStatusIndb } = require("./requestApprovalForProduct");
require('dotenv').config();
const requestAllApprovalForProduct = async ()=>{
    const token = await getAccessToken();
    let findQuery ={}
    const url =
    process.env.GENBA_BASE_URL + '/products/submitpreliveapprovalrequest';
    findQuery.Srp = {$exists:true}
    const result =  await GenbaProdModel.find(findQuery)
    result.forEach(async function(product){
      if (product.PreLiveState === 0) { 
        const body = {
            ClientTransactionID: product._id,
            Sku: product.Sku,
            Url: `${process.env.BASE_URL}`+`${product._id}`,
          };
          try {
            const res = await axios({
              method: 'POST',
              url,
              headers: {
                Authorization: `Bearer ${token}`,
              },
              data: body,
            });

            await updatePreLiveStatusIndb(res.data?.Sku, res.data?.PreLiveState)
           
            
          } catch (error) {
            console.log('err msg from requestAllApproval ==>', error.message);
            return false;
          }
      }
    })
    return true;
}
module.exports = requestAllApprovalForProduct