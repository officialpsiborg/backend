require('dotenv').config();
const {v4: uuid} = require('uuid');
const axios = require('axios');
const getAccessToken = require('./getAccessToken');
const GenbaProdModel = require('../../models/GenbaProdModel');

const requestApprovalForProduct = async (sku,id) => {
  const token = await getAccessToken();

  const url =
    process.env.GENBA_BASE_URL + '/products/submitpreliveapprovalrequest';
  const body = {
    ClientTransactionID: uuid(),
    Sku: sku,
    Url: process.env.BASE_URL + `gamedetail/${id}`,
  };
  console.log('body  submitpreliveapprovalrequest  ==>', body);
  try {
    const res = await axios({
      method: 'POST',
      url,
      headers: {
        Authorization: `Bearer ${token}`,
      },
      data: body,
    });
    console.log('res submitpreliveapprovalrequest ===>', res.data);
    await updatePreLiveStatusIndb(res.data?.Sku, res.data?.PreLiveState)
    return {
      error: false,
      liveState: res.data?.PreLiveState,
      sku: res.data?.Sku,
    };
  } catch (error) {
    return {
      error: true,
      errorMessage: error.response?.data?.Details,
    };
    // console.error('err res status', error?.response?.status);
    // console.error('err res data', error?.response?.data);
    // console.log('err msg', error.message);
  }
};

async function updatePreLiveStatusIndb (sku, liveState) {
  let state = liveState
  if (liveState == 0) state = 2
  if (liveState == 2) state = 4
  if (liveState == 3) state = 0
  if (liveState == 4) state = 3
  await GenbaProdModel.findOneAndUpdate({Sku: sku}, {PreLiveState: state })
}

module.exports = {requestApprovalForProduct, updatePreLiveStatusIndb};
