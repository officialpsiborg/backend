const mongoose = require('mongoose');
const ApplicationDataSchema = mongoose.Schema(
  {
    orderTax: {type: Number, default: 10}, //10% tax
    termsAndConditions: {type: String, default: 'hey'},
  },
  {
    collection: 'ApplicationData',
  }
);

module.exports = mongoose.model('ApplicationData', ApplicationDataSchema);
