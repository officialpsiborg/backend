const mongoose = require('mongoose');

const PromostionSchema =  mongoose.Schema({
    Name:{type:String},
    PromotionId:{type:String},
    From:{type:String},
    To:{type:String},
    PromotionItems:[{Sku:{type:String},
    CurrencyCode:{type:String},
        DiscountPercentage:{type:Number },
         OriginalSRP:{type:Number},
        OriginalWSP:{type:Number},
        DiscountedSRP:{type:Number},
        DiscountedWSP:{type:Number}}],
    expireAt :{type:Date}              
},{timestamps:true})

module.exports = mongoose.model('Promotions', PromostionSchema);