const mongoose = require("mongoose");
const RefreshTokenSchema = mongoose.Schema(
  {
    refreshToken: { type: String, required: true },
    createdAt: { type: Date, default: Date.now() },
    expiredIn: { type: String },
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt",
    },
    collection: "RefreshTokens",
  }
);

module.exports = mongoose.model("RefreshToken", RefreshTokenSchema);
