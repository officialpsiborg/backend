const mongoose = require('mongoose');
const GameCategorySchema = mongoose.Schema(
  {
    Name: {type: String, required: true, unique: true},
    NumberOfGames: {type: Number},
    Order: {type: Number, required: true},
    Games: [{type: mongoose.Schema.Types.ObjectId, ref: 'GenbaProduct'}],
  },
  {
    collection: 'GameCategories',
  }
);

module.exports = mongoose.model('GameCategory', GameCategorySchema);
