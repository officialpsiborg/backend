const mongoose = require('mongoose');
const crypto = require('crypto');
const UserSchema = mongoose.Schema(
  {
    firstName: {type: String, required: true},
    lastName: {type: String, required: false},
    email: {type: String, required: true, unique: true},
    password: {type: String, required: true},
    convertPrice: {type: Number, required: false},
    phone: {type: String},
    gamesOwned: {type: Number, default: 0},
    verified: {type: Boolean, default: false},
    role: {type: String, default: 'user'},
    google: {
      id: {
        type: String,
      },
      name: {
        type: String,
      },
      email: {
        type: String,
      },
    },
    method: {type: String, required: false},
    transactions: [{type: mongoose.Schema.Types.ObjectId, ref: 'Transaction'}],
    wishlist: [{type: mongoose.Schema.Types.ObjectId, ref: 'GenbaProduct'}],
    cart: {
      subtotal: {type: Number, default: 0}, //change on add and remove
      totalItem: {type: Number, default: 0}, //inc/dec on add and remove
      items: [{type: mongoose.Schema.Types.ObjectId, ref: 'GenbaProduct'}],
    },
    refreshToken: {type: String, default: ''},
    disabled: {type: Boolean, default: false},
    resetPasswordToken:{type:String},
    resetPasswordExpires: {type: Date},
  },
  {
    timestamps: {
      createdAt: 'createdAt',
      updatedAt: 'updatedAt',
    },
    collection: 'Users',
  }
);

UserSchema.methods.getResetPasswordToken = function getResetPasswordToken(){
  const resetToken = crypto.randomBytes(20).toString("hex");
  // Hash token (private key) and save to database
  this.resetPasswordToken = crypto
    .createHash("sha256")
    .update(resetToken)
    .digest("hex");
  // Set token expire date
  this.resetPasswordExpire = Date.now() + 20 * (60 * 1000);
  return this.resetPasswordToken;
}

module.exports = mongoose.model('User', UserSchema);
