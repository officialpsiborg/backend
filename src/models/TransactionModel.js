const mongoose = require("mongoose");
const TransectionSchema = mongoose.Schema(
  {
    txn_oid: { type: String, required: true },
    txn_id: { type: String },
    txn_amt: { type: Number, required: true },
    user_name: { type: String },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    //  prod_name: [{type: String}],
    //  prod_sku: [{type: String, required: true}],
    product: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "GenbaProduct",
        required: true,
      },
    ],

    prod_key: [{ type: String }],
    //initial, pending, delivered,
    prod_key_status: { type: String, default: "initial" },
    //initial ,pending , challenge , failure , success ,
    txn_status: { type: String, default: "initial" },
    isDelivered: { type: Boolean, default: false },
    // isFromCart
    // isBundleTxn
    //
    prod: [
      {
        unid: { type: String },
        productId: {
          type: mongoose.Schema.Types.ObjectId,
          ref: "GenbaProduct",
        },
        Sku: {type: String},
        Name: { type: String },
        amount: { type: Number},
        prod_key: { type: String },
        prod_key_status: { type: String },
      },
    ],
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt",
    },
    collection: "Transactions",
  }
);

module.exports = mongoose.model("Transaction", TransectionSchema);
