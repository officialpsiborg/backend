const mongoose = require('mongoose');

const GenbaProdSchema = mongoose.Schema(
  {
    Sku: {type: String, required: true, unique: true},
    Name: {type: String, required: true},
    Developer: {type: String},
    Publisher: {type: String},
    Genres: [{type: String}], //array of strings
    ProductDiscription: {type: String},
    Platform: {type: String},
    RegionCode: {type: String, required: true},
    ReleaseDate: {type: String},
    DigitalReleaseDate: {type: String, required: true},
    LegalText: {type: String},
    PreLiveState: {type: Number},
    KeyProvider: {type: String, required: true},
    KeyActivationInstruction: {type: String},
    IsBundle: {type: Boolean, required: true},
    BundleProductsSku: [{type: String}],
    MinimumRequirement: {type: String},
    RecommendedRequirement: {type: String},
    WhitelistedCountries: [{type: String}],
    BlacklistedCountries: [{type: String}],
    Images: [
      {
        _id: false,
        Name: {type: String},
        Type: {type: String},
        Size: {type: Number},
        Extension: {type: String},
        Height: {type: Number},
        Width: {type: Number},
        URL: {type: String},
      },
    ],
    Videos: [
      {
        _id: false,
        URL: {type: String},
        Thumbnail: {type: String},
        Provider: {type: String},
      },
    ],
    Srp: {type: Number},
    Wsp: {type: Number},
    Stock: {type: Boolean, default: false},
    banner: {type: String},
    //------------------------
    Price: {type: Number}, //
    isAdminPromotion: {type: Boolean, default:false},
    onPromotionDiscount: {type:Number,default:0},
    onPromotion:{type:Boolean,default:false},
    onPromotionPrice:{type:Number,default:0},
    onPromotionSrp:{type:Number,default:0},
    onPromotionTo:{type:Date},
    onPromotionFrom:{type:Date},
    finalPrice: {type: Number},
    finalDiscount: {type: Number, default: 0},
    Discount: {type: Number, default: 0}, //
    KeySold: {type: Number, default: 0}, //
    Disabled: {type: Boolean, default: false}, //default: true => later in production it will be true by default and manually enabled by admin
    isNewProd: {type: Boolean, default: true},
    isModifiedProd: {type: Boolean, default: false},
    PreReleaseDate:{type:String},
    EstimatedReleaseDate:{type:String}
  },
  {
    timestamps: {
      createdAt: 'createdAt',
      updatedAt: 'updatedAt',
    },
    collection: 'GenbaProducts',
  }
);
module.exports = mongoose.model('GenbaProduct', GenbaProdSchema);

// GenbaProdSchema.pre('findOneAndUpdate', (next) => {
//   this.options.runValidators = true;
//   // this.options.setDefaultsOnInsert = true;
//   next();
// });
