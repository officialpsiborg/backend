const finalErrorHandler = (error, req, res, next) => {
  console.log('finalErrorHandler', error.statusCode, error.message);
  let statusCode = error.statusCode;
  let errorMsg = error.message;

  if (error.code === 31254) {
    // select query field error handler
    statusCode = 400;
    errorMsg = 'Invalid Request Parameter.';
  }
  res.status(statusCode || 500);
  res.json({
    Error: true,
    Message: errorMsg || 'Something went wrong',
    ErrorMessage: error.message,
  });
};

module.exports = finalErrorHandler;
