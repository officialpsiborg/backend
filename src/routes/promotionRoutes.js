const express = require('express');
const {
    getPromotion,featurePromotionalGames
  } = require('../controllers/Promotions');


const promotionRoute = express.Router()

promotionRoute.get('/promotions',getPromotion)
promotionRoute.get('/featurePromotional',featurePromotionalGames)


module.exports = promotionRoute;