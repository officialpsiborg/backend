const express = require('express');
const userAuthRoute = express.Router();
const {
  registerUser,
  loginUser,
  newAccessToken,
  logoutUser,
  forgotPassword,
  verifyEmail
} = require('../controllers/userAuthControllers');

userAuthRoute.post('/register', registerUser);
userAuthRoute.post('/login', loginUser);
userAuthRoute.get('/newtoken/:refresh_token', newAccessToken);
userAuthRoute.delete('/logout/:refresh_token', logoutUser);
userAuthRoute.post('/forgotPassword',forgotPassword);
userAuthRoute.get('/verifyEmail/:verifyEmailToken', verifyEmail);

module.exports = userAuthRoute;
