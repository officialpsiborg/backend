const express = require('express');
const genbaGamesRoutes = express.Router();
const 
    getGenbaGames
= require('../controllers/genbaControllers/getGames');
const {authenticateUser} = require('../middlewares/auth.middleware');

// make payment for current sku
genbaGamesRoutes.get('/genbaGamesList', getGenbaGames);

module.exports = genbaGamesRoutes;
