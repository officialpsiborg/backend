const express = require("express");
const buyProductRoutes = express.Router();
const {
  makePayment,
  getProductKey,
  makePaymentforSingleProduct,
} = require("../controllers/buyProductControllers");
const { authenticateUser } = require("../middlewares/auth.middleware");

// make payment for current sku
buyProductRoutes.get("/makepayment", authenticateUser, makePayment);
buyProductRoutes.get(
  "/makePaymentforSingleProduct/:productId",
  authenticateUser,
  makePaymentforSingleProduct
);

// order produt ket for current sku and current txnoid
buyProductRoutes.get("/getkey/:txnOid", authenticateUser, getProductKey);

module.exports = buyProductRoutes;
