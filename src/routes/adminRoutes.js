const express = require('express');
const {
  getProducts,
  updateProduct,
  addCategory,
  updateCategory,
  requestApproval,
  getTransactions,
  getUsers,
  addFeaturedGames,
  getSoldkeys,
  getCategory,
  disableUser,
  deleteCategory,
  getFigures,
  updateAdmin,
  getNoGames,
  getNoOrders,
  totalSale,
  recentOrders,
  getNoUser,
  deleteGameInCategory,
  updateDisableProduct,
  passwordChange,
  getfilterProduct,
  getLiveProduct,
  getIdelProduct,
  graph,
  gameTypeCount,
  requestAllGames,
  addThumbnail,
  convertPriceInDb,
  deleteUser
} = require('../controllers/adminControllers');
const getAdminProfile = require('../controllers/adminControllers/getAdminProfile');



const {authenticateAdmin} = require('../middlewares/auth.middleware');
const adminRoutes = express.Router();

adminRoutes.get('/products', authenticateAdmin, getProducts); //all products with some admin fields
adminRoutes.patch('/products/:_id', authenticateAdmin, updateProduct); // update all products
adminRoutes.patch('/products', authenticateAdmin, updateProduct); // update all products
adminRoutes.get('/product/soldkeys', authenticateAdmin, getSoldkeys); // get key sold for a product
adminRoutes.get('/getcategory', authenticateAdmin, getCategory);
adminRoutes.post('/category', authenticateAdmin, addCategory); // add category by only admin
adminRoutes.delete('/delCategory/:id', authenticateAdmin, deleteCategory);
adminRoutes.patch('/category/:_id', authenticateAdmin, updateCategory); // update category by admin
adminRoutes.post('/featuredgames', authenticateAdmin, addFeaturedGames); // add featured games
adminRoutes.get('/transactions', authenticateAdmin, getTransactions); // get all transactions dynamic
adminRoutes.get('/getusers', authenticateAdmin, getUsers); // get all user dynamic
adminRoutes.get('/getAdminProfile', authenticateAdmin, getAdminProfile); // get admin Profile
adminRoutes.patch('/disableuser', authenticateAdmin, disableUser);
adminRoutes.get('/requestproduct', authenticateAdmin, requestApproval); //request for a product to be approved
adminRoutes.get('/requestAllproducts', authenticateAdmin, requestAllGames); // request All product to be approved
adminRoutes.get('/getFigures', authenticateAdmin, getFigures); // get all figures
adminRoutes.patch('/adminupdate', authenticateAdmin,updateAdmin) //update admin details
adminRoutes.get('/games',authenticateAdmin,getNoGames) //get all games 
adminRoutes.get('/noorders',authenticateAdmin,getNoOrders);
adminRoutes.get('/sales',authenticateAdmin,totalSale);
adminRoutes.get('/recentOrders', authenticateAdmin,recentOrders);
adminRoutes.get('/users',authenticateAdmin,getNoUser);
adminRoutes.delete('/deleteGamesInCategory/:id',authenticateAdmin,deleteGameInCategory);
adminRoutes.patch('/productDisable/:id',authenticateAdmin,updateDisableProduct);
//adminRoutes.patch('/changepasswords',authenticateAdmin,passwordChange);
adminRoutes.patch('/changecred',authenticateAdmin,passwordChange)
adminRoutes.get('/filterProduct',authenticateAdmin,getfilterProduct)
adminRoutes.get('/getLiveProduct',authenticateAdmin,getLiveProduct)
adminRoutes.get('/getIdelProduct',authenticateAdmin,getIdelProduct)
adminRoutes.post('/addBanner', authenticateAdmin, addThumbnail)
adminRoutes.get('/graph',graph);
adminRoutes.get('/gametypes', gameTypeCount);
adminRoutes.post('/priceConverter', authenticateAdmin, convertPriceInDb);
adminRoutes.delete('/user/:userId', authenticateAdmin, deleteUser)
module.exports = adminRoutes;



// getCategory
