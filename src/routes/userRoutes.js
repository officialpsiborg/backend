const express = require('express');
const {
  updateUserDetails,
  getTransactions,
  getUserDetails,
  getOrders,
  changePassword,
  addToWishlist,
  getWishlist,
  removeFromWishlist,
  addToCart,
  removeFromCart,
  getCart,
  cartCheckout,
  getUserWithToken
} = require('../controllers/userControllers');
const {authenticateUser} = require('../middlewares/auth.middleware');
const userRoute = express.Router();

userRoute.get('/getdetails', authenticateUser, getUserDetails);
userRoute.get('/getUserWithToken', authenticateUser, getUserWithToken);
userRoute.get('/orders', authenticateUser, getOrders);
userRoute.patch('/updatedetails', authenticateUser, updateUserDetails);
userRoute.patch('/changepassword', authenticateUser, changePassword);
userRoute.get('/getTransactions', authenticateUser, getTransactions);
userRoute.get('/wishlist', authenticateUser, getWishlist);
userRoute.post('/wishlist/:gameId', authenticateUser, addToWishlist);
userRoute.delete('/wishlist/:gameId', authenticateUser, removeFromWishlist);
userRoute.post('/cart/:gameId', authenticateUser, addToCart);
userRoute.delete('/cart/:gameId', authenticateUser, removeFromCart);
userRoute.get('/cart', authenticateUser, getCart);
userRoute.get('/cart/checkout', authenticateUser, cartCheckout);
module.exports = userRoute;
