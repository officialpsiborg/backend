const express = require('express');
const {
  getStock,
  getProducts,
  getCategory,
  getFeaturedGames,
  getSimilarProducts,
  getBundleGames,
  getDetailedGame,
  getUpcomingGames,
  getGameFilter
} = require('../controllers/productControllers');
const productRoute = express.Router();

productRoute.get('/', getProducts);
productRoute.get('/getstock/:Sku', getStock);
productRoute.get('/getcategory', getCategory);
productRoute.get('/getfeaturedgames', getFeaturedGames);
productRoute.get('/getSimilar', getSimilarProducts);
productRoute.get('/getBundleGames/:id', getBundleGames);
productRoute.get('/gameDetailed/:id',getDetailedGame);
productRoute.get('/getUpcomingGames', getUpcomingGames);
productRoute.get('/getGameFilter', getGameFilter);


module.exports = productRoute;
