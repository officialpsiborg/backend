const ApiError = require('../error-handler/ApiError');

const unspecifiedRoutesHandler = (req, res, next) => {
  next(new ApiError('Route Not Found', 404));
};

module.exports = unspecifiedRoutesHandler;
