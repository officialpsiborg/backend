const express = require('express');
const {authenticateUser} = require('../middlewares/auth.middleware');
const testingRoute = express.Router();

testingRoute.get('/ping', authenticateUser, (req, res, next) => {
  try {
    res
      .status(200)
      .json({Error: false, status: 200, message: 'Success', User: req.user});
  } catch (error) {
    next(error);
  }
});

module.exports = testingRoute;
