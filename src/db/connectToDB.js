const mongoose = require('mongoose');

const connectToDB = async (URI) => {
  const dbPromise = new Promise((resolve, reject) => {
    mongoose
      .connect(URI)
      .then(() => {
        resolve({message: 'Connected to MongoDB', error: false});
      })
      .catch((err) => {
        reject({message: err.message, error: true});
      });
  });
  return dbPromise;
};

module.exports = connectToDB;
