require("dotenv").config();
const lodash = require("lodash");
const _ = require("lodash");
const axios = require("axios");
const GenbaProduct = require("../models/GenbaProdModel");

const {
  getAccessToken,
  getStockOfProduct,
} = require("../services/genbaServices");
const convertUSDToIDR = require("../helper/convertUSDToIDR");

let continuationToken = null;
let totalGames = 0;
let finalRes = [];
let token;
const fetchProductFromGenba = async () => {
  totalGames = 0;
  finalRes = [];
  console.log("Start Feating Product");
  token = await getAccessToken();
  if (token) console.log("token is found");
  do {
    await getProduct();
  } while (continuationToken);
};
async function getProduct() {
  console.log("Starting the get product call");
  const url =
    process.env.GENBA_BASE_URL +
    "/products?includeMeta=true&countryCode=ID&etailerid=339b90a8-9202-4d3b-8699-f3b0b182299f" +
    (continuationToken ? `&continuationtoken=${continuationToken}` : "");
  try {
    const res = await axios({
      method: "GET",
      url: url,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    //  console.log("res,from fetch product call",res)
    //   console.log("data product product call,",res.data.Products)
    // console.log("how many products ==>",res.data.Products.length)
    totalGames += res.data.Products.length;
    continuationToken = res.data.ContinuationToken;
    finalRes.push(res.data.Products);
    if (!continuationToken) {
      console.log("No continuationToken Found");
      console.log("total games ==>", totalGames);
      continuationToken = null;
      return false;
    }

    await checkingGames(res.data.Products);
  } catch (e) {
    console.log("Error in getting genba product", e);
    continuationToken = null;
  }
}
async function checkingGames(products) {
  // console.log("saving games",products)
  // return
  try {
    // products.forEach(async (product) => {
    for (let product of products) {
      const prod = await filterProduct(product);
      const allowedProduct =
        (prod.WhitelistedCountries &&
          prod.WhitelistedCountries.includes("ID")) ||
        (prod.BlacklistedCountries &&
          !prod.BlacklistedCountries.includes("ID"));
      // const allowedProduct = (!prod.BlacklistedCountries.includes("ID"));
      if (allowedProduct) {
        const { Srp, Wsp, Price, finalPrice } = await fetchSrpAndWsp(prod.Sku);
        const Stock = await getStockOfProduct(prod.Sku);
        // console.log("prices return and stock status",Srp,Wsp,Price,Stock)
        await updateProduct({ ...prod, Srp, Wsp, Price, finalPrice, Stock });
      }
    }
  } catch (error) {
    console.log("error while savingGenbaProducts", error.message);
    continuationToken = null;
  }
}
async function updateProduct(product) {
  try {
    // console.log("in Updating product.........", product);

    // if (product.Sku == "754090df-c7b8-42a0-a197-9cfcd89d69bd") {
    //   console.log("===== Found Product =====")
    //   console.log(product)
    //   console.log("===== Found Product =====")
    // }
    const foundProd = await GenbaProduct.findOne({ Sku: product.Sku });
    if (foundProd) {
      // if (product.Sku == "754090df-c7b8-42a0-a197-9cfcd89d69bd") {
      //   console.log('foundProd EXist Found++', product.Sku);
      // }
      const isEqual = isEqualProduct(product, foundProd);
      if (!isEqual) {
        // if (product.Sku == "754090df-c7b8-42a0-a197-9cfcd89d69bd") {
        //   console.log("===== Modified Found Product =====")
        //   console.log(product)
        //   console.log('Modified product Found:', product.Sku);
        //   console.log('Saving modified product...');
        //   console.log("===== Modified Found Product =====");
        // }
        await GenbaProduct.findOneAndUpdate(
          { Sku: foundProd.Sku },
          {
            ...product,
            finalDiscount: foundProd.Discount,
            onPromotion: false,
            onPromotionDiscount: 0,
            onPromotionSrp: 0,
            onPromotionPrice: 0,
            isModifiedProd: true,
          }
        );
        let Price = product.Srp;
        if (foundProd.finalDiscount > 0) {
          Price = Math.round(
            product.Srp - (product.Srp * foundProd.finalDiscount) / 100
          );
        }

        // if (product.Sku === "754090df-c7b8-42a0-a197-9cfcd89d69bd") {
        //   console.log("product.Srp , final Price ==> ", product.Srp , Price)
        // }
        await GenbaProduct.findOneAndUpdate(
          { Sku: foundProd.Sku },
          { Price: Price, finalPrice: Price }
        );
        // console.log("found product ===================================>>",foundProd);
        //console.log("fond updated product =====================================>",foundUpdated);
      } else {
        await GenbaProduct.findOneAndUpdate(
          { Sku: foundProd.Sku },
          {
            ...product,
            onPromotion: false,
            onPromotionDiscount: 0,
            onPromotionSrp: 0,
            onPromotionPrice: 0,
          }
        );
      }

      // else {
      //   console.log("else not equal")
      //   if (product.Sku == "754090df-c7b8-42a0-a197-9cfcd89d69bd") {
      //     console.log("===== Else Found Product =====")
      //     console.log(product)
      //     console.log("===== Else Found Product =====");
      //   }
      // }
    }

    if (!foundProd) {
      // if (product.Sku == "754090df-c7b8-42a0-a197-9cfcd89d69bd") {
      //   console.log('New Product Found++', product.Sku);
      // }
      const newProd = new GenbaProduct({
        ...product,
      });
      // console.log('Saving New Product...');
      await newProd.save();
    }
  } catch (error) {
    console.log("error while upserting products", error);
  }
}

async function filterProduct(product) {
  const {
    Sku,
    Name,
    Developer,
    Publisher,
    Genres,
    Platform,
    RegionCode,
    ReleaseDate,
    DigitalReleaseDate,
    PreLiveState,
    IsBundle,
    PreReleaseDate,
    EstimatedReleaseDate,
  } = product;

  const descriptionLeagalObj = product.Languages.find(
    (obj) => obj.LanguageName === "English"
  );
  let ProductDiscription = descriptionLeagalObj?.LocalisedDescription;
  let LegalText = descriptionLeagalObj?.LegalText;

  const BundleProductsSku =
    product.BundleProducts?.map((obj) => obj.SkuID) || [];

  const KeyProvider = product.KeyProvider.Name;
  const KeyActivationInstruction = product.KeyProvider.Instructions.find(
    (ins) => ins.Language === "English"
  ).Value;

  const MinimumRequirement = product.MetaData.find(
    (obj) => obj.ParentCategory === "Minimum Requirements"
  )?.Values[0];
  const RecommendedRequirement = product.MetaData.find(
    (obj) => obj.ParentCategory === "Recommended Requirements"
  )?.Values[0];

  const WhitelistedCountries = product.Restrictions?.WhitelistCountryCodes;
  const BlacklistedCountries = product.Restrictions?.BlacklistCountryCodes;
  const Images = getImagesList(product);
  const Videos = getVideosList(product);
  return {
    Sku,
    Name,
    Developer,
    Publisher,
    Genres,
    ProductDiscription,
    Platform,
    RegionCode,
    ReleaseDate,
    DigitalReleaseDate,
    LegalText,
    PreLiveState,
    KeyProvider,
    KeyActivationInstruction,
    IsBundle,
    BundleProductsSku,
    MinimumRequirement,
    RecommendedRequirement,
    WhitelistedCountries,
    BlacklistedCountries,
    Images,
    Videos,
    EstimatedReleaseDate,
    PreReleaseDate,
  };
}
async function fetchSrpAndWsp(Sku) {
  const token = await getAccessToken();
  const url = process.env.GENBA_BASE_URL + `/prices?sku=${Sku}`;
  try {
    const res = await axios({
      method: "GET",
      url: url,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    const Prices = res.data.Prices;

    // Prices  =  [
    //   {
    //     Sku: '754090df-c7b8-42a0-a197-9cfcd89d69bd',
    //     Product: 'Against the Storm - Early Access',
    //     RegionCode: 'SEA',
    //     DefaultStatus: 0,
    //     CurrencyCode: 'IDR',
    //     Wsp: 116726.49,
    //     Srp: 149999,
    //     IsPromotion: false
    //   },
    //   {
    //     Sku: '754090df-c7b8-42a0-a197-9cfcd89d69bd',
    //     Product: 'Against the Storm - Early Access',
    //     RegionCode: 'SEA',
    //     DefaultStatus: 1,
    //     CurrencyCode: 'USD',
    //     Wsp: 17.11,
    //     Srp: 19.99,
    //     IsPromotion: false
    //   }
    // ]

    const obj = Prices.find(
      (obj) => obj.CurrencyCode == "IDR" || obj.CurrencyCode == "USD"
    );
    if (!obj) {
      console.log("This product has not IDR currency:", Sku);
    }
    // console.log(obj, "obj");

    if (obj.CurrencyCode === "USD") {
      console.log("SKU To convert Currency ===>", Sku, obj);
      obj.Srp = await convertUSDToIDR(obj?.Srp);
      obj.Wsp = await convertUSDToIDR(obj?.Wsp);
    }

    return {
      Srp: obj?.Srp ? obj?.Srp : 0,
      Wsp: obj?.Wsp,
      Price: obj?.Srp,
      finalPrice: obj?.Srp,
    };
  } catch (error) {
    console.log("error while fetching srp wsp", error.message);
    console.log("========== Error ==========");
    console.log("SKU ==>", Sku);
    console.log("========== Error ==========");
  }
}

function getImagesList(product) {
  let images = [];
  images = product?.Graphics?.map((imgObj) => {
    return {
      Name: imgObj.FileName,
      Type: imgObj.GraphicType,
      Size: imgObj.FileSize,
      Extension: imgObj.FileName.split(".").pop(),
      Height: imgObj.OriginalHeight,
      Width: imgObj.OriginalWidth,
      URL: imgObj.ImageUrl,
    };
  });
  return images;
}

function getVideosList(product) {
  let videos = [];
  videos = product.VideoURLs?.map((vidObj) => {
    let videoURL;
    if (vidObj.Provider === "YouTube") {
      videoURL =
        "https://www.youtube.com/watch?v=" + vidObj.URL.split("/").pop();
    }

    return {
      URL: videoURL,
      Thumbnail: vidObj.PosterFrameURL,
      Provider: vidObj.Provider,
    };
  });
  return videos;
}
function isEqualProduct(prod1, prod2) {
  let isEqual = true;
  const newProd1 = {
    Sku: prod1.Sku,
    Name: prod1.Name,
    Srp: prod1.Srp,
    Wsp: prod1.Wsp,
    ProductDiscription: prod1.ProductDiscription,
    LegalText: prod1.LegalText,
    RegionCode: prod1.RegionCode,
    Genres: prod1.Genres, //
    Publisher: prod1.Publisher,
    PreLiveState: prod1.PreLiveState,
    Developer: prod1.Developer,
    KeyProvider: prod1.KeyProvider,
    KeyActivationInstruction: prod1.KeyActivationInstruction,
    IsBundle: prod1.IsBundle,
    BundleProductsSku: prod1.BundleProductsSku, //
    MinimumRequirement: prod1.MinimumRequirement,
    RecommendedRequirement: prod1.RecommendedRequirement,
    WhitelistedCountries: prod1?.WhitelistedCountries || [],
    BlacklistedCountries: prod1?.BlacklistedCountries || [],
    Stock: prod1.Stock,
    finalPrice: prod1.finalPrice,
    // Images: prod1.Images, //
    // Videos: prod1.Videos, //
  };
  const newProd2 = {
    Sku: prod2.Sku,
    Name: prod2.Name,
    Srp: prod2.Srp,
    Wsp: prod2.Wsp,
    ProductDiscription: prod2.ProductDiscription,
    LegalText: prod2.LegalText,
    RegionCode: prod2.RegionCode,
    Genres: prod2.Genres,
    Publisher: prod2.Publisher,
    PreLiveState: prod2.PreLiveState,
    Developer: prod2.Developer,
    KeyProvider: prod2.KeyProvider,
    KeyActivationInstruction: prod2.KeyActivationInstruction,
    IsBundle: prod2.IsBundle,
    BundleProductsSku: prod2.BundleProductsSku,
    MinimumRequirement: prod2.MinimumRequirement,
    RecommendedRequirement: prod2.RecommendedRequirement,
    WhitelistedCountries: prod2?.WhitelistedCountries || [],
    BlacklistedCountries: prod2?.BlacklistedCountries || [],
    Stock: prod2.Stock,
    finalPrice: prod2.finalPrice,
    // Images: prod2.Images,
    // Videos: prod2.Videos,
  };

  if (!_.isEqual(newProd1, newProd2)) {
    isEqual = false;
  }

  prod1.Images.forEach((imgObj, i) => {
    let img1 = {
      Name: imgObj.Name || "",
      Type: imgObj.Type || "",
      Size: imgObj.Size || 0,
      Extension: imgObj.Extension || "",
      Height: imgObj.Height || 0,
      Width: imgObj.Width || 0,
      URL: imgObj.URL || "",
    };
    let img2 = {
      Name: prod2.Images[i].Name || "",
      Type: prod2.Images[i].Type || "",
      Size: prod2.Images[i].Size || 0,
      Extension: prod2.Images[i].Extension || "",
      Height: prod2.Images[i].Height || 0,
      Width: prod2.Images[i].Width || 0,
      URL: prod2.Images[i].URL || "",
    };

    if (!_.isEqual(img1, img2)) {
      // console.log("Img Obj Missmatch", img1, img2);
      isEqual = false;
    }
  });

  prod1.Videos.forEach((vidObj, i) => {
    let vid1 = {
      URL: vidObj.URL || "",
      Thumbnail: vidObj.Thumbnail || "",
      Provider: vidObj.Provider || "",
    };
    let vid2 = {
      URL: prod2.Videos[i].URL || "",
      Thumbnail: prod2.Videos[i].Thumbnail || "",
      Provider: prod2.Videos[i].Provider || "",
    };
    if (!_.isEqual(vid1, vid2)) {
      // console.log("Video Object Missmatch", vid1, vid2);
      isEqual = false;
    }
  });

  return isEqual;
}

module.exports = fetchProductFromGenba;
