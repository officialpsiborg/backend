const axios = require("axios");
const GenbaProduct = require("../models/GenbaProdModel");
const Promotions = require("../models/Promotions");
require("dotenv").config();
const { getAccessToken } = require("../services/genbaServices");
const convertUSDToIDR = require("../services/genbaServices");
let continuationToken = null;
const fetchPromotionFromGenba = async () => {
  console.log("Start Feating Product");
  do {
    await getPromotionFromGenba();
  } while (continuationToken);
};
const getPromotionFromGenba = async () => {
  const fromDate = new Date(Date.now() + 3600 * 1000 * 24)
    .toISOString()
    .split("T")[0];
  const toDate = new Date(Date.now() + 3600 * 1000 * 24)
    .toISOString()
    .split("T")[0];
  console.log("on promotions ", fromDate);
  const token = await getAccessToken();
  const url =
    process.env.GENBA_BASE_URL +
    "/promotions?" +
    `fromDate=${fromDate}&toDate=${toDate}`;
  // console.log("URL we are hitting",url)
  try {
    const result = await axios({
      method: "GET",
      url: url,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    // console.log("response from Genba ",result.data);
    await checkingGames(result.data.Promotions);
  } catch (e) {
    console.log("Error in Promotion get method", e);
    continuationToken = null;
  }
};
async function checkingGames(Promotions) {
  console.log("checking Games");
  try {
    // Promotions.forEach(async (promotion) => {
    for (let promotion of Promotions) {
      // await savingPromotion(promotion)
      await savingPromotionToDb(promotion);
      console.log("Promotion ==>", promotion);
    }
    // console.log("Promotions fetching Done  ==>", P)
  } catch (e) {
    console.log("Error in the promotions", e);
  }
}
// async function savingPromotion(promotion){
//     try{
//      //   console.log("saving promotion ")
//         const foundPromotion =  await Promotions.findOne({PromotionId:promotion.PromotionId})
//      //   console.log("found promotion========>",promotion)
//         let To = promotion.To
//         let From =  promotion.From
//         promotion.PromotionItems.forEach( async (item)=>{
//             if(item.CurrencyCode === "IDR"){

//                 let promotionDiscountPrice = (item.OriginalSRP - (item.OriginalSRP * (item.DiscountPercentage/100)))

//                 // if admin percentage
//                 let adminDiscountPrice = (promotionDiscountPrice - (promotionDiscountPrice* (adminDiscount/100)))
//                 let finalDiscount = (adminDiscountPrice/item.OriginalSRP) * 100
//                 await GenbaProduct.findOneAndUpdate(
//                     {Sku:item.Sku},
//                     {onPromotion:true,
//                     onPromotionDiscount:item.DiscountPercentage,
//                     onPromotionSrp:item.OriginalSRP,
//                     onPromotionPrice:item.DiscountedSRP,
//                     onPromotionTo:To,
//                     onPromotionFrom:From,
//                     finalPrice:  ,
//                     finalDiscount: finalDiscount
//                     })
//        //             console.log("in the loop")
//             }
//         })
//         if(foundPromotion){
//          //   console.log("found the promotion now modifying the promotion")
//             await Promotions.findOneAndUpdate({PromotionId:foundPromotion.PromotionId},{...promotion})
//         }else{
//             await Promotions.create({
//                 Name:promotion.Name,
//                 Promotions:promotion.PromotionId,
//                 From:promotion.From,
//                 To:promotion.To,
//                 PromotionItems:[...promotion.PromotionItems],
//                 expireAt:promotion.To

//             })
//         }

//     }catch(e){
//         console.log("Error in the promotion")
//     }
// }

async function savingPromotionToDb(promotion) {
  try {
    console.log("saving promotion ");
    // const foundPromotion =  await GenbaProduct.findOne({PromotionId:promotion.PromotionId})
    // console.log("found promotion========>", promotion);
    let To = promotion.To;
    let From = promotion.From;
    // promotion.PromotionItems.forEach(async (item) => {
    for (let item of promotion.PromotionItems) {
      if (item.CurrencyCode === "IDR") {
        let totalGame = await GenbaProduct.findOne({ Sku: item.Sku });

        let promotionDiscountPrice = item.DiscountedSRP;

        let adminDiscountPrice = item.DiscountedSRP;
        let finalDiscount = item.DiscountPercentage;
        if (totalGame) {
          if (totalGame.isAdminPromotion) {
            // if admin percentage
            adminDiscountPrice =
              promotionDiscountPrice -
              promotionDiscountPrice * (totalGame.finalDiscount / 100);
            finalDiscount = (adminDiscountPrice / item.OriginalSRP) * 100;
          }

          await GenbaProduct.findOneAndUpdate(
            { Sku: item.Sku },
            {
              onPromotion: true,
              onPromotionDiscount: item.DiscountPercentage,
              onPromotionSrp: item.OriginalSRP,
              onPromotionPrice: item.DiscountedSRP,
              onPromotionTo: To,
              onPromotionFrom: From,
              finalPrice: Math.round(adminDiscountPrice),
              finalDiscount: finalDiscount,
            }
          );
        }

        //             console.log("in the loop")
      } else if (item.CurrencyCode === "USD") {
        let totalGame = await GenbaProduct.findOne({ Sku: item.Sku });
        if (totalGame && totalGame.RegionCode === "WW") {
          let promotionDiscountPrice = await convertUSDToIDR(
            item.DiscountedSRP
          );
          let adminDiscountPrice = await convertUSDToIDR(item.DiscountedSRP);
          let finalDiscount = item.DiscountPercentage;
          let OriginalSRP = await convertUSDToIDR(item.OriginalSRP);
          let DiscountedSRP = await convertUSDToIDR(item.DiscountedSRP);

          if (totalGame.isAdminPromotion) {
            // if admin percentage
            adminDiscountPrice =
              promotionDiscountPrice -
              promotionDiscountPrice * (totalGame.finalDiscount / 100);
            finalDiscount = (adminDiscountPrice / OriginalSRP) * 100;
          }

          await GenbaProduct.findOneAndUpdate(
            { Sku: item.Sku },
            {
              onPromotion: true,
              onPromotionDiscount: item.DiscountPercentage,
              onPromotionSrp: OriginalSRP,
              onPromotionPrice: DiscountedSRP,
              onPromotionTo: To,
              onPromotionFrom: From,
              finalPrice: Math.round(adminDiscountPrice),
              finalDiscount: finalDiscount,
            }
          );
        }
      }
    }
  } catch (e) {
    console.log("Error in the promotion");
  }
}
module.exports = fetchPromotionFromGenba;
