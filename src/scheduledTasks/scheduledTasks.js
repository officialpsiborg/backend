const mapProductsToGenba = require("./mapProductsToGenba");
const schedule = require("node-schedule");
const RefreshToken = require("../models/RefreshTokenModel");
const GenbaProduct = require("../models/GenbaProdModel");
const fetchProductFromGenba = require("../genbaProductServices/ProductScheduler");
const fetchPromotionFromGenba = require("../genbaProductServices/PromotionScheduler");

const scheduledTasks = async () => {
  schedule.scheduleJob("38 13 * * *", async () => {
    console.log("=== Scheduled tasks log ===");
    //
    await fetchProductFromGenba();
    await fetchPromotionFromGenba();
    //mapProductsToGenba();
    //  genbaProdMapJob.cancel(); //remove before production here this is for one time npm
  });
  const deleteExpiredRefreshTokenJob = schedule.scheduleJob(
    "@hourly",
    async () => {
      const result = await RefreshToken.findOneAndRemove({});
    }
  );
};

module.exports = scheduledTasks;

//result.Price = Math.floor(Math.floor(Math.random() * 10000) + 100);
// const savedPrd = await result.save();
