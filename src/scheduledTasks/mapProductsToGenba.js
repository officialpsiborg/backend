require('dotenv').config();
const lodash = require('lodash');
const _ = require('lodash');
const axios = require('axios');
const GenbaProduct = require('../models/GenbaProdModel');
const {
  getAccessToken,
  getStockOfProduct,
} = require('../services/genbaServices');

let continuationToken = null;
const mapProductsToGenba = async () => {
  console.log('Mapping Genba products...');
  // return;
  do {
    await fetchGenbaProducts();
  } while (continuationToken);
};

async function fetchGenbaProducts() {
  const token = await getAccessToken();
  console.log('Fetching Genba Products...');
  const url =
    process.env.GENBA_BASE_URL +
    '/products?includeMeta=true' +
    (continuationToken ? `&continuationtoken=${continuationToken}` : '');
  try {
    const res = await axios({
      method: 'GET',
      url: url,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    continuationToken = res.data.ContinuationToken;
    if (!continuationToken) console.log('No continuationToken Found');
    await analyseProducts(res.data.Products);
  } catch (error) {
    console.log('error while fetching Genba Products', error.message);
    continuationToken = null;
  }
}

async function analyseProducts(products) {
  console.log('Saving Genba products...');
  try {
    await products.forEach(async (product) => {
      const prod =  filterProductFields(product);
      const allowedProduct =
        (prod.WhitelistedCountries &&
          prod.WhitelistedCountries.includes('ID')) ||
        (prod.BlacklistedCountries &&
          !prod.BlacklistedCountries.includes('ID'));
      if (allowedProduct) {
        const obj=  await fetchSrpAndWsp(prod.Sku);
        const Stock = await getStockOfProduct(prod.Sku);
        await upsertProduct({...prod,obj, Stock});
      }
    });
  } catch (error) {
    console.log('error while savingGenbaProducts', error.message);
    continuationToken = null;
  }
}

async function upsertProduct(product) {
  try {
    const foundProd = await GenbaProduct.findOne({Sku: product.Sku});
    if (foundProd) {
      const isEqual = isEqualProduct(product, foundProd);
      if (!isEqual) {
        console.log('Modified product Found:', product.Sku);
        console.log('Saving modified product...');
        await GenbaProduct.findOneAndUpdate(
          {Sku: foundProd.Sku},
          {
            ...product,
            isModifiedProd: true,
          }
        );
      }
    }

    if (!foundProd) {
      console.log('New Product Found:', product.Sku);
      const newProd = new GenbaProduct({
        ...product,
      });
      console.log('Saving New Product...');
      await newProd.save();
    }
  } catch (error) {
    console.log('error while upserting products', error);
  }
}

function filterProductFields(product) {
  const {
    Sku,
    Name,
    Developer,
    Publisher,
    Genres,
    Platform,
    RegionCode,
    ReleaseDate,
    DigitalReleaseDate,
    PreLiveState,
    IsBundle,
  } = product;

  const descriptionLeagalObj = product.Languages.find(
    (obj) => obj.LanguageName === 'English'
  );
  let ProductDiscription = descriptionLeagalObj?.LocalisedDescription;
  let LegalText = descriptionLeagalObj?.LegalText;

  const BundleProductsSku =
    product.BundleProducts?.map((obj) => obj.SkuID) || [];

  const KeyProvider = product.KeyProvider.Name;
  const KeyActivationInstruction = product.KeyProvider.Instructions.find(
    (ins) => ins.Language === 'English'
  ).Value;

  const MinimumRequirement = product.MetaData.find(
    (obj) => obj.ParentCategory === 'Minimum Requirements'
  )?.Values[0];
  const RecommendedRequirement = product.MetaData.find(
    (obj) => obj.ParentCategory === 'Recommended Requirements'
  )?.Values[0];

  const WhitelistedCountries = product.Restrictions?.WhitelistCountryCodes;
  const BlacklistedCountries = product.Restrictions?.BlacklistCountryCodes;
  const Images = getImagesList(product);
  const Videos = getVideosList(product);
  return {
    Sku,
    Name,
    Developer,
    Publisher,
    Genres,
    ProductDiscription,
    Platform,
    RegionCode,
    ReleaseDate,
    DigitalReleaseDate,
    LegalText,
    PreLiveState,
    KeyProvider,
    KeyActivationInstruction,
    IsBundle,
    BundleProductsSku,
    MinimumRequirement,
    RecommendedRequirement,
    WhitelistedCountries,
    BlacklistedCountries,
    Images,
    Videos,
  };
}

function getImagesList(product) {
  let images = [];
  images = product?.Graphics?.map((imgObj) => {
    return {
      Name: imgObj.FileName,
      Type: imgObj.GraphicType,
      Size: imgObj.FileSize,
      Extension: imgObj.FileName.split('.').pop(),
      Height: imgObj.OriginalHeight,
      Width: imgObj.OriginalWidth,
      URL: imgObj.ImageUrl,
    };
  });
  return images;
}

function getVideosList(product) {
  let videos = [];
  videos = product.VideoURLs?.map((vidObj) => {
    let videoURL;
    if (vidObj.Provider === 'YouTube') {
      videoURL =
        'https://www.youtube.com/watch?v=' + vidObj.URL.split('/').pop();
    }

    return {
      URL: videoURL,
      Thumbnail: vidObj.PosterFrameURL,
      Provider: vidObj.Provider,
    };
  });
  return videos;
}

async function fetchSrpAndWsp(Sku) {
  const token = await getAccessToken();
  const url = process.env.GENBA_BASE_URL + `/prices?sku=${Sku}`;
  try {
    const res = await axios({
      method: 'GET',
      url: url,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const Prices = res.data.Prices;
    const obj = Prices.find((obj) => obj.CurrencyCode == 'IDR');
    

    if (!obj) {
      console.log('This product has not IDR currency:', Sku);
    }

  
     return {
      Srp: obj?.Srp,
      Wsp: obj?.Wsp,
      Price: obj?.Srp
    };
  } catch (error) {
    console.log('error while fetching srp wsp', error.message);
  }
}

function isEqualProduct(prod1, prod2) {
  let isEqual = true;
  const newProd1 = {
    Sku: prod1.Sku,
    Name: prod1.Name,
    Srp: prod1.Srp,
    Wsp: prod1.Wsp,
    ProductDiscription: prod1.ProductDiscription,
    LegalText: prod1.LegalText,
    RegionCode: prod1.RegionCode,
    Genres: prod1.Genres, //
    Publisher: prod1.Publisher,
    PreLiveState: prod1.PreLiveState,
    Developer: prod1.Developer,
    KeyProvider: prod1.KeyProvider,
    KeyActivationInstruction: prod1.KeyActivationInstruction,
    IsBundle: prod1.IsBundle,
    BundleProductsSku: prod1.BundleProductsSku, //
    MinimumRequirement: prod1.MinimumRequirement,
    RecommendedRequirement: prod1.RecommendedRequirement,
    WhitelistedCountries: prod1?.WhitelistedCountries || [],
    BlacklistedCountries: prod1?.BlacklistedCountries || [],
    // Images: prod1.Images, //
    // Videos: prod1.Videos, //
  };
  const newProd2 = {
    Sku: prod2.Sku,
    Name: prod2.Name,
    Srp: prod2.Srp,
    Wsp: prod2.Wsp,
    ProductDiscription: prod2.ProductDiscription,
    LegalText: prod2.LegalText,
    RegionCode: prod2.RegionCode,
    Genres: prod2.Genres,
    Publisher: prod2.Publisher,
    PreLiveState: prod2.PreLiveState,
    Developer: prod2.Developer,
    KeyProvider: prod2.KeyProvider,
    KeyActivationInstruction: prod2.KeyActivationInstruction,
    IsBundle: prod2.IsBundle,
    BundleProductsSku: prod2.BundleProductsSku,
    MinimumRequirement: prod2.MinimumRequirement,
    RecommendedRequirement: prod2.RecommendedRequirement,
    WhitelistedCountries: prod2?.WhitelistedCountries || [],
    BlacklistedCountries: prod2?.BlacklistedCountries || [],
    // Images: prod2.Images,
    // Videos: prod2.Videos,
  };

  if (!_.isEqual(newProd1, newProd2)) {
    isEqual = false;
  }

  prod1.Images.forEach((imgObj, i) => {
    let img1 = {
      Name: imgObj.Name || '',
      Type: imgObj.Type || '',
      Size: imgObj.Size || 0,
      Extension: imgObj.Extension || '',
      Height: imgObj.Height || 0,
      Width: imgObj.Width || 0,
      URL: imgObj.URL || '',
    };
    let img2 = {
      Name: prod2.Images[i].Name || '',
      Type: prod2.Images[i].Type || '',
      Size: prod2.Images[i].Size || 0,
      Extension: prod2.Images[i].Extension || '',
      Height: prod2.Images[i].Height || 0,
      Width: prod2.Images[i].Width || 0,
      URL: prod2.Images[i].URL || '',
    };

    if (!_.isEqual(img1, img2)) {
      console.log('Img Obj Missmatch', img1, img2);
      isEqual = false;
    }
  });
  prod1.Videos.forEach((vidObj, i) => {
    let vid1 = {
      URL: vidObj.URL || '',
      Thumbnail: vidObj.Thumbnail || '',
      Provider: vidObj.Provider || '',
    };
    let vid2 = {
      URL: prod2.Videos[i].URL || '',
      Thumbnail: prod2.Videos[i].Thumbnail || '',
      Provider: prod2.Videos[i].Provider || '',
    };
    if (!_.isEqual(vid1, vid2)) {
      console.log('Video Object Missmatch', vid1, vid2);
      isEqual = false;
    }
  });

  return isEqual;
}

module.exports = mapProductsToGenba;

// const foundProd = await GenbaProduct.findOne({Sku: prod.Sku});
// if (foundProd) {
//   console.log(new Date(foundProd.updatedAt).getTime());
//   console.log('name sku', foundProd.Sku, foundProd.Name);
// }
