const schedule = require("node-schedule");
const GenbaProdModel = require("../models/GenbaProdModel");


const scheduleDeleteFromPromotion =  async ()=>{
    schedule.scheduleJob("* * * * *", async()=>{
        const result = await GenbaProdModel.find({onPromotion:true})
        //console.log("fetch data in schedule",result)
        result.forEach(async (item)=>{
            schedule.scheduleJob(result.PromotionTo,async()=>{
                console.log("in nested scheduler")
                await GenbaProdModel.findOneAndUpdate({Sku:item.Sku},{
                    onPromotion:false,
                    onPromotionDiscount:0,
                    onPromotionSrp:0,
                    onPromotionPrice:0,
                    onPromotionTo:0,
                    onPromotionFrom:0
                })
            })
        })
    })

}


module.exports = scheduleDeleteFromPromotion