const axios = require("axios");
const UserModel = require("../models/UserModel");

async function getExchangeRate() {
  try {
    const response = await axios.get(
      `https://openexchangerates.org/api/latest.json?app_id=${process.env.APP_ID}`
    );
    const exchangeRate = response.data.rates.IDR / response.data.rates.USD;
    return exchangeRate;
  } catch (error) {
    console.log("Error from convert USD =>", error.message);
  }
}

async function convertUSDToIDR(usdAmount) {
  // const exchangeRate = await getExchangeRate();    //63eccb72d72e6f26f71577ce
  // By default taking convertPrice from admin Account  // 63ce683df08626d903c6a500
  const userAmount = await UserModel.findById(
    "63eccb72d72e6f26f71577ce"
  ).lean();
  // const userAmount = await UserModel.findById(
  //   "63ce683df08626d903c6a500"
  // ).lean();//local

  const idrAmount = usdAmount * Number(userAmount.convertPrice);
  console.log(
    "usdAmount, userAmount , idrAmount ==>",
    usdAmount,
    Number(userAmount.convertPrice),
    idrAmount
  );
  return idrAmount.toFixed(2);
}

module.exports = convertUSDToIDR;
