const express = require('express');
const app = express();
const morgan = require('morgan');
const cors = require('cors');
const finalErrorHandler = require('./src/error-handler/finalErrorHandler');
const unspecifiedRoutesHandler = require('./src/routes/unspecifiedRoutesHandler');
const userAuthRoutes = require('./src/routes/userAuthRoutes');
const userRoute = require('./src/routes/userRoutes');
const adminRoutes = require('./src/routes/adminRoutes');
const buyProductRoutes = require('./src/routes/buyProductRoutes');
const promotionRoute = require('./src/routes/promotionRoutes');
const productRoute = require('./src/routes/productRoutes');
const testingRoute = require('./src/routes/testingRoutes');
const genbaGamesRoutes = require('./src/routes/genbaGamesRoutes');
const { application } = require('express');
const { requestAllApprovalForProduct } = require('./src/services/genbaServices');
const invoice = require('./src/services/genbaServices/invoice/invoice');
const passport = require("passport");
const genAccessToken = require('./src/controllers/userAuthControllers/genAccessToken');
const webHookRoute = require('./src/webhooks/webhook-routes');
const sendInVoice = require('./src/controllers/buyProductControllers/sendInvoice');
require("./src/services/oauth/googleOauth")(passport);

//required middlewares to wark the backend
app.use(cors("*")); // handling cross origin resource sharing configuration
app.use(express.json()); // parse request data in json object
app.use(express.urlencoded({extended: true})); //urlencoded request data will be available
app.use(morgan('dev')); // request logger for development
/********************************************************************/
console.log("New User")
//specified routes
app.use('/api/test', testingRoute); // testing route for ping to server
app.use('/api/product', productRoute);
app.use('/api/user', userRoute);
app.use('/api/authUser', userAuthRoutes);
app.use('/api/buy', buyProductRoutes);
app.use('/api/admin', adminRoutes);
app.use('/api/promo',promotionRoute);
app.use('/api/genbaGames', genbaGamesRoutes);
// app.use('/api/product',productRoute)

app.use('/api/test',(req,res)=>{
    res.status(200).send("backend is running")
})

app.use('/webhook', webHookRoute);

// ==================== Google ====================== //

app.get('/auth/google',
  passport.authenticate('google', { scope: ["email", "profile"]  }));

app.get('/auth/google/callback', 
  passport.authenticate('google', { session: false }),

  function(req, res) {
    const token = genAccessToken(
      {
        _id: req.user._id,
        email: req.user.email,
        role: req.user.role,
      },
      process.env.JWT_EXPIRE
    );
    res.redirect(`https://mckey.io/check/${token}`);
  }
  );

/********************************************************************/
//unspecified routes handler
app.use(unspecifiedRoutesHandler); //all the unknown requests are handled here

//final error handler
app.use(finalErrorHandler); //all the errors will be handled here which is occured in above routes

require('./testing/test'); // for testing the data and initializing the database in development
//invoice()
//requestAllApprovalForProduct()


// sendInVoice("rahul.kumar@psiborg.in") //"rahul.kumar@psiborg.in"  //"sudhanshu.sharma@psiborg.in"

module.exports = app;
