const GenbaProduct = require('../src/models/GenbaProdModel');
const User = require('../src/models/UserModel');
const _ = require('lodash');

const getNewProducts = async () => {
  const newProducts = await GenbaProduct.find({isNewProd: true}).select(
    '-_id Name'
  );
  console.log('newProducts\n', newProducts.length);
};
// getNewProducts();
//
//
const reviewNewProducts = async () => {
  const result = await GenbaProduct.updateMany(
    {isNewProd: true},
    {
      isNewProd: false,
    }
  ).select('-_id Name');
  console.log('reviewNewProducts\n', result);
};
// reviewNewProducts();
//
//
const getModifiedProducts = async () => {
  const modifiedProducts = await GenbaProduct.find({
    isModifiedProd: true,
  }).select('-_id Name Sku');
  console.log('modifiedProducts\n', modifiedProducts.length);
};
// getModifiedProducts();
//
//
const reviewModifiedProducts = async () => {
  const result = await GenbaProduct.updateMany(
    {isModifiedProd: true},
    {
      isModifiedProd: false,
    }
  ).select('-_id Name');
  console.log('reviewModifiedProducts\n', result);
};
// reviewModifiedProducts();
//
//
//--------------------------------------------------------
// fillin Price with random Prices from 100 to 10000
//----------------------------------------------------------
const priceFillingForProducts = async () => {
  const prodLength = await GenbaProduct.find({}).count();
  for (let i = 0; i < prodLength + 1; i++) {
    const prod = await GenbaProduct.find({})
      .skip(i)
      .limit(1)
      .select('_id Name Price');
    if (prod.length > 0) {
      prod[0].Price = Math.floor(Math.floor(Math.random() * 10000) + 100);
      res = await prod[0].save();
      continue;
    } else {
      console.count('no prod: ');
    }
  }
};
// priceFillingForProducts();

//
//

//
//

//
//
//
//-------------------------------------
// console.log('test lodash', _.isEqual(a, b));

// const result = await GenbaProduct.find({
//   updatedAt: {
//     $gt: new Date('2022-07-26T12:22:37.108+00:00').toISOString(),
//   },
// }).select('-_id Name');
// console.log(result, result.length);

//find products of array length
//{"$expr":{$gt:[{$size:"$BlacklistedContries"},0]}}
